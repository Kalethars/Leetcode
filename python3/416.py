# 给你一个 只包含正整数 的 非空 数组 nums 。请你判断是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。
# 示例 1：
# 输入：nums = [1,5,11,5]
# 输出：true
# 解释：数组可以分割成 [1, 5, 5] 和 [11] 。
# 示例 2：
# 输入：nums = [1,2,3,5]
# 输出：false
# 解释：数组不能分割成两个元素和相等的子集。
# 提示：
# 1 <= nums.length <= 200
# 1 <= nums[i] <= 100

from typing import List


class Solution:
    def canPartition(self, nums: List[int]) -> bool:
        total = sum(nums)
        if total % 2 != 0:
            return False
        can_reach = [True] + [False] * (total // 2)
        for num in nums:
            for value in range(total // 2, num - 1, -1):
                can_reach[value] |= can_reach[value - num]
            if can_reach[-1]:
                return True
        return False


if __name__ == '__main__':
    assert Solution().canPartition([1, 5, 11, 5]) is True
    assert Solution().canPartition([1, 2, 3, 5]) is False
    assert Solution().canPartition([1, 5, 10, 6]) is True
    assert Solution().canPartition([1, 5, 10, 8]) is False
