# 给你两个二进制字符串，返回它们的和（用二进制表示）。
# 输入为 非空 字符串且只包含数字 1 和 0。
# 示例 1:
# 输入: a = "11", b = "1"
# 输出: "100"
# 示例 2:
# 输入: a = "1010", b = "1011"
# 输出: "10101"
# 提示：
# 每个字符串仅由字符 '0' 或 '1' 组成。
# 1 <= a.length, b.length <= 10^4
# 字符串如果不是 "0" ，就都不含前导零。

class Solution:
    def addBinary(self, a: str, b: str) -> str:
        if len(a) < len(b):
            a, b = b, a
        b = '0' * (len(a) - len(b)) + b
        result, carrier = [], 0
        for digit_a, digit_b in zip(a[::-1], b[::-1]):
            result.append(str(int(digit_a) ^ int(digit_b) ^ carrier))
            carrier = int(digit_a) + int(digit_b) + carrier >= 2
        if carrier:
            result.append('1')
        return ''.join(result[::-1])


if __name__ == '__main__':
    assert Solution().addBinary('11', '1') == '100'
    assert Solution().addBinary('1010', '1011') == '10101'
    assert Solution().addBinary('111', '1010') == '10001'
