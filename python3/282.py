# 给定一个仅包含数字 0-9 的字符串和一个目标值，在数字之间添加 二元 运算符（不是一元）+、- 或 * ，返回所有能够得到目标值的表达式。
# 示例 1:
# 输入: num = "123", target = 6
# 输出: ["1+2+3", "1*2*3"] 
# 示例 2:
# 输入: num = "232", target = 8
# 输出: ["2*3+2", "2+3*2"]
# 示例 3:
# 输入: num = "105", target = 5
# 输出: ["1*0+5","10-5"]
# 示例 4:
# 输入: num = "00", target = 0
# 输出: ["0+0", "0-0", "0*0"]
# 示例 5:
# 输入: num = "3456237490", target = 9191
# 输出: []
# 提示：
# 0 <= num.length <= 10
# num 仅含数字

from copy import deepcopy
from functools import lru_cache
from typing import Dict, List


class Solution:
    def addOperators(self, num: str, target: int) -> List[str]:
        return self.find_all_combos(num).get(target, [])

    @lru_cache(None)
    def find_all_combos(self, num: str, noplusminus=False, nomultiply=False) -> Dict[int, List[str]]:
        if len(num) == 0:
            return dict()
        elif nomultiply and noplusminus:
            all_combos = {int(num): [num]} if num == '0' or num[0] != '0' else {}
        elif noplusminus:
            all_combos = deepcopy(self.find_all_combos(num, True, True))
            for index in range(1, len(num)):
                all_left_combos = self.find_all_combos(num[:index], True, True)
                all_right_combos = self.find_all_combos(num[index:], True)
                for left_value, left_combos in all_left_combos.items():
                    for right_value, right_combos in all_right_combos.items():
                        all_combos[left_value * right_value] = all_combos.get(left_value * right_value, []) + [
                            f'{left_combo}*{right_combo}'
                            for left_combo in left_combos for right_combo in right_combos
                        ]
        else:
            all_combos = deepcopy(self.find_all_combos(num, True))
            for index in range(1, len(num)):
                all_left_combos = self.find_all_combos(num[:index], True)
                all_right_combos = self.find_all_combos(num[index:])
                for left_value, left_combos in all_left_combos.items():
                    for right_value, right_combos in all_right_combos.items():
                        all_combos[left_value + right_value] = all_combos.get(left_value + right_value, []) + [
                            f'{left_combo}+{right_combo}'
                            for left_combo in left_combos for right_combo in right_combos
                        ]
                        all_combos[left_value - right_value] = all_combos.get(left_value - right_value, []) + [
                            f'{left_combo}-{right_combo.replace("+", " ").replace("-", "+").replace(" ", "-")}'
                            for left_combo in left_combos for right_combo in right_combos
                        ]
        return all_combos


if __name__ == '__main__':
    assert len(Solution().addOperators('123', 6)) == 2
    assert len(Solution().addOperators('232', 8)) == 2
    assert len(Solution().addOperators('105', 5)) == 2
    assert len(Solution().addOperators('00', 0)) == 3
    assert len(Solution().addOperators('3456237490', 9191)) == 0
    assert len(Solution().addOperators('123456789', 45)) == 121
