# 给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
# 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        head = 0
        for index, num in enumerate(nums):
            if index == 0 or num != nums[index - 1]:
                nums[head] = num
                head += 1
        return head


if __name__ == '__main__':
    assert Solution().removeDuplicates([1, 1, 2]) == 2
    assert Solution().removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4]) == 5
