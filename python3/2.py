# 给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。
# 如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。
# 您可以假设除了数字 0 之外，这两个数都不会以 0 开头。

from python3.listnode import ListNode


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        result = ListNode(0)
        pointer = result
        multiplier, carry = 1, 0
        while l1 is not None or l2 is not None or carry > 0:
            if carry == 0 and (l1 is None or l2 is None):
                pointer.next = l2 if l1 is None else l1
                return result.next
            v1, l1 = (l1.val, l1.next) if l1 is not None else (0, l1)
            v2, l2 = (l2.val, l2.next) if l2 is not None else (0, l2)
            pointer.next, carry = ListNode((v1 + v2 + carry) % 10), (v1 + v2 + carry) // 10
            multiplier *= 10
            pointer = pointer.next
        return result.next


if __name__ == '__main__':
    assert Solution().addTwoNumbers(
        ListNode.build_chain(2, 4, 3),
        ListNode.build_chain(5, 6, 4)
    ).get_chain() == [7, 0, 8]
