# 已知存在一个按非降序排列的整数数组 nums ，数组中的值不必互不相同。
# 在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转 ，
# 使数组变为 [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。
# 例如， [0,1,2,4,4,4,5,6,6,7] 在下标 5 处经旋转后可能变为 [4,5,6,6,7,0,1,2,4,4] 。
# 给你 旋转后 的数组 nums 和一个整数 target ，请你编写一个函数来判断给定的目标值是否存在于数组中。
# 如果 nums 中存在这个目标值 target ，则返回 true ，否则返回 false 。
# 示例 1：
# 输入：nums = [2,5,6,0,0,1,2], target = 0
# 输出：true
# 示例 2：
# 输入：nums = [2,5,6,0,0,1,2], target = 3
# 输出：false
# 提示：
# 1 <= nums.length <= 5000
# -10^4 <= nums[i] <= 10^4
# 题目数据保证 nums 在预先未知的某个下标上进行了旋转
# -10^4 <= target <= 10^4

from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        return len(nums) > 0 and (
                target in [nums[0], nums[-1], nums[len(nums) // 2]] or
                (not (nums[-1] < nums[len(nums) // 2] < target) and self.search(nums[:len(nums) // 2], target)) or
                (not (nums[0] > nums[len(nums) // 2] > target) and self.search(nums[len(nums) // 2 + 1:], target))
        )


if __name__ == '__main__':
    assert Solution().search([2, 5, 6, 0, 0, 1, 2], 0) is True
    assert Solution().search([2, 5, 6, 0, 0, 1, 2], 3) is False
