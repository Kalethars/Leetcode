# 给你一个字符串 s 、一个字符串 t 。返回 s 中涵盖 t 所有字符的最小子串。如果 s 中不存在涵盖 t 所有字符的子串，则返回空字符串 "" 。
# 注意：
# 对于 t 中重复字符，我们寻找的子字符串中该字符数量必须不少于 t 中该字符数量。
# 如果 s 中存在这样的子串，我们保证它是唯一的答案。
# 示例 1：
# 输入：s = "ADOBECODEBANC", t = "ABC"
# 输出："BANC"
# 示例 2：
# 输入：s = "a", t = "a"
# 输出："a"
# 示例 3:
# 输入: s = "a", t = "aa"
# 输出: ""
# 解释: t 中两个字符 'a' 均应包含在 s 的子串中，
# 因此没有符合条件的子字符串，返回空字符串。
# 提示：
# 1 <= s.length, t.length <= 10^5
# s 和 t 由英文字母组成

class Solution:
    def minWindow(self, s: str, t: str) -> str:
        if len(s) < len(t):
            return ''
        counter = dict()
        for char in t:
            counter[char] = counter.get(char, 0) - 1
        missing_count = len(counter)
        head = 0
        min_string = ''
        for tail, tail_char in enumerate(s):
            if tail_char in counter:
                counter[tail_char] += 1
                if counter[tail_char] == 0:
                    missing_count -= 1
            if missing_count == 0:
                while missing_count == 0:
                    if s[head] in counter:
                        counter[s[head]] -= 1
                        if counter[s[head]] == -1:
                            missing_count += 1
                    head += 1
                if len(min_string) == 0 or len(min_string) > tail - head + 2:
                    min_string = s[head - 1:tail + 1]
        return min_string


if __name__ == '__main__':
    assert Solution().minWindow('ADOBECODEBANC', 'ABC') == 'BANC'
    assert Solution().minWindow('a', 'a') == 'a'
    assert Solution().minWindow('a', 'aa') == ''
    assert Solution().minWindow('a', 'b') == ''
