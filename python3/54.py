# 给你一个 m 行 n 列的矩阵 matrix ，请按照 顺时针螺旋顺序 ，返回矩阵中的所有元素。
# 示例 1：
# 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
# 输出：[1,2,3,6,9,8,7,4,5]
# 示例 2：
# 输入：matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
# 输出：[1,2,3,4,8,12,11,10,9,5,6,7]
# 提示
# m == matrix.length
# n == matrix[i].length
# 1 <= m, n <= 10
# -100 <= matrix[i][j] <= 100

from typing import List


class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        if len(matrix) == 0 or len(matrix[0]) == 0:
            return []
        elif len(matrix) == 1 or len(matrix[0]) == 1:
            return sum(matrix, [])
        else:
            return matrix[0] + [matrix[i][-1] for i in range(1, len(matrix) - 1)] + \
                   matrix[-1][::-1] + [matrix[i][0] for i in range(1, len(matrix) - 1)][::-1] + \
                   self.spiralOrder([line[1:-1] for line in matrix[1:-1]])


if __name__ == '__main__':
    assert Solution().spiralOrder([[1, 2, 3], [4, 5, 6], [7, 8, 9]]) == [1, 2, 3, 6, 9, 8, 7, 4, 5]
    assert Solution().spiralOrder([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]) == \
           [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]
