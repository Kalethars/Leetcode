# 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。

from python3.listnode import ListNode


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        head = node = ListNode(None)
        while l1 is not None or l2 is not None:
            if l1 is not None and (l2 is None or l1.val < l2.val):
                node.next = ListNode(l1.val)
                l1 = l1.next
            else:
                node.next = ListNode(l2.val)
                l2 = l2.next
            node = node.next
        return head.next


class Solution2:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 is None or l2 is None:
            return l1 or l2
        if l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else:
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2


if __name__ == '__main__':
    assert Solution().mergeTwoLists(
        ListNode.build_chain(1, 2, 4),
        ListNode.build_chain(1, 3, 4)
    ).get_chain() == [1, 1, 2, 3, 4, 4]
    assert Solution2().mergeTwoLists(
        ListNode.build_chain(1, 2, 4),
        ListNode.build_chain(1, 3, 4)
    ).get_chain() == [1, 1, 2, 3, 4, 4]
