# 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。

from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        self.patterns = {1: {'()'}}
        return list(self.generate(n))

    def generate(self, n: int):
        if n not in self.patterns:
            results = set()
            for i in range(1, n // 2 + 1):
                minor_results, major_results = self.generate(i), self.generate(n - i)
                for minor_result in minor_results:
                    for major_result in major_results:
                        results.add(minor_result + major_result)
                        results.add(major_result + minor_result)
                        if i == 1:
                            results.add(f'({major_result})')
            self.patterns[n] = results
        return self.patterns[n]


if __name__ == '__main__':
    assert set(Solution().generateParenthesis(3)) == {
        '((()))',
        '(()())',
        '(())()',
        '()(())',
        '()()()'
    }
