# 设计并实现一个算法，找出二叉树中某两个节点的第一个共同祖先。不得将其他的节点存储在另外的数据结构中。注意：这不一定是二叉搜索树。
# 例如，给定如下二叉树: root = [3,5,1,6,2,0,8,null,null,7,4]
#
#     3
#    / \
#   5   1
#  / \ / \
# 6  2 0  8
#   / \
#  7   4
# 示例 1:
# 输入: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
# 输出: 3
# 解释: 节点 5 和节点 1 的最近公共祖先是节点 3。
# 示例 2:
# 输入: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
# 输出: 5
# 解释: 节点 5 和节点 4 的最近公共祖先是节点 5。因为根据定义最近公共祖先节点可以为节点本身。
# 说明:
# 所有节点的值都是唯一的。
# p、q 为不同节点且均存在于给定的二叉树中。

from python3.treenode import TreeNode


class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        return self.ancestor(root, p, q)[0]

    def ancestor(self, tree: TreeNode, p: TreeNode, q: TreeNode):
        if tree is None:
            return None, False, False
        left_ancestor, left_contains_p, left_contains_q = self.ancestor(tree.left, p, q)
        right_ancestor, right_contains_p, right_contains_q = self.ancestor(tree.right, p, q)
        contains_p = left_contains_p or right_contains_p or tree == p
        contains_q = left_contains_q or right_contains_q or tree == q
        return left_ancestor or right_ancestor or (tree if contains_p and contains_q else None), contains_p, contains_q


if __name__ == '__main__':
    tree = TreeNode.build_tree([3, 5, 1, 6, 2, 0, 8, None, None, 7, 4])
    assert Solution().lowestCommonAncestor(tree, tree.left, tree.right) == tree
    assert Solution().lowestCommonAncestor(tree, tree.left, tree.left.right.right) == tree.left
