# 编写一个函数来查找字符串数组中的最长公共前缀。
# 如果不存在公共前缀，返回空字符串 ""。

from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ''
        elif len(strs) == 1:
            return strs[0]
        min_length = min(len(s) for s in strs)
        for i in range(min_length):
            if any(s[i] != strs[0][i] for s in strs):
                return strs[0][:i]
        return strs[0][:min_length]


if __name__ == '__main__':
    assert Solution().longestCommonPrefix(['flower', 'flow', 'flight']) == 'fl'
    assert Solution().longestCommonPrefix(['dog', 'racecar', 'car']) == ''
