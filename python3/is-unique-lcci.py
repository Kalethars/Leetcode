# 实现一个算法，确定一个字符串 s 的所有字符是否全都不同。
# 示例 1：
# 输入: s = "leetcode"
# 输出: false
# 示例 2：
# 输入: s = "abc"
# 输出: true
# 限制：
# 0 <= len(s) <= 100
# 如果你不使用额外的数据结构，会很加分。

class Solution:
    def isUnique(self, astr: str) -> bool:
        if len(astr) > 26:
            return False
        astr = astr + ' ' * (26 - len(astr))
        index = 0
        while index < len(astr):
            while astr[index] != ' ':
                correct_index = ord(astr[index]) - ord('a')
                if index == correct_index:
                    break
                if astr[correct_index] == astr[index]:
                    return False
                index1, index2 = min(index, correct_index), max(correct_index, index)
                astr = astr[:index1] + astr[index2] + astr[index1 + 1:index2] + astr[index1] + astr[index2 + 1:]
            index += 1
        return True


if __name__ == '__main__':
    assert Solution().isUnique('leetcode') is False
    assert Solution().isUnique('abc') is True
