# 实现 int sqrt(int x) 函数。
# 计算并返回 x 的平方根，其中 x 是非负整数。
# 由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。
# 示例 1:
# 输入: 4
# 输出: 2
# 示例 2:
# 输入: 8
# 输出: 2
# 说明: 8 的平方根是 2.82842..., 
#      由于返回类型是整数，小数部分将被舍去。

class Solution:
    def mySqrt(self, x: int) -> int:
        left, right = 0, x
        while left < right:
            middle = (left + right) // 2
            if middle * middle <= x and (middle + 1) * (middle + 1) > x:
                return middle
            elif middle * middle > x:
                right = middle - 1
            else:
                left = middle + 1
        return left

    def mySqrt_newton(self, x: int) -> int:
        if x <= 1:
            return x
        sqrt = x
        while not (x - 1e-5 < sqrt * sqrt <= x + 1e-5):
            sqrt = (sqrt + x / sqrt) / 2
        return int(sqrt)


if __name__ == '__main__':
    assert Solution().mySqrt(4) == 2
    assert Solution().mySqrt(8) == 2
    assert Solution().mySqrt(0) == 0
    assert Solution().mySqrt(1) == 1
