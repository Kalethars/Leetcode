# 给你一个 无重叠的 ，按照区间起始端点排序的区间列表。
# 在列表中插入一个新的区间，你需要确保列表中的区间仍然有序且不重叠（如果有必要的话，可以合并区间）。
# 示例 1：
# 输入：intervals = [[1,3],[6,9]], newInterval = [2,5]
# 输出：[[1,5],[6,9]]
# 示例 2：
# 输入：intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
# 输出：[[1,2],[3,10],[12,16]]
# 解释：这是因为新的区间 [4,8] 与 [3,5],[6,7],[8,10] 重叠。
# 示例 3：
# 输入：intervals = [], newInterval = [5,7]
# 输出：[[5,7]]
# 示例 4：
# 输入：intervals = [[1,5]], newInterval = [2,3]
# 输出：[[1,5]]
# 示例 5：
# 输入：intervals = [[1,5]], newInterval = [2,7]
# 输出：[[1,7]]
# 提示：
# 0 <= intervals.length <= 10^4
# intervals[i].length == 2
# 0 <= intervals[i][0] <= intervals[i][1] <= 10^5
# intervals 根据 intervals[i][0] 按 升序 排列
# newInterval.length == 2
# 0 <= newInterval[0] <= newInterval[1] <= 10^5

import bisect
from typing import List


class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        if len(intervals) == 0:
            return [newInterval]
        left_border = bisect.bisect_left([interval[1] for interval in intervals], newInterval[0])
        right_border = bisect.bisect_right([interval[0] for interval in intervals], newInterval[1]) - 1
        if left_border <= right_border:
            return intervals[:left_border] + \
                   [[min(intervals[left_border][0], newInterval[0]),
                     max(intervals[right_border][1], newInterval[1])]] + \
                   intervals[right_border + 1:]
        elif right_border < 0:
            return [newInterval] + intervals
        elif left_border >= len(intervals):
            return intervals + [newInterval]
        else:
            return intervals[:left_border] + [newInterval] + intervals[right_border + 1:]


if __name__ == '__main__':
    assert Solution().insert([[1, 3], [6, 9]], [2, 5]) == [[1, 5], [6, 9]]
    assert Solution().insert([[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]], [4, 8]) == [[1, 2], [3, 10], [12, 16]]
    assert Solution().insert([], [5, 7]) == [[5, 7]]
    assert Solution().insert([[1, 5]], [2, 3]) == [[1, 5]]
    assert Solution().insert([[1, 5]], [2, 7]) == [[1, 7]]
    assert Solution().insert([[1, 3], [6, 9]], [4, 5]) == [[1, 3], [4, 5], [6, 9]]
    assert Solution().insert([[4, 5], [6, 9]], [1, 3]) == [[1, 3], [4, 5], [6, 9]]
    assert Solution().insert([[1, 3], [4, 5]], [6, 9]) == [[1, 3], [4, 5], [6, 9]]
    assert Solution().insert([[1, 3], [4, 5]], [5, 9]) == [[1, 3], [4, 9]]
    assert Solution().insert([[4, 5], [6, 9]], [1, 4]) == [[1, 5], [6, 9]]
