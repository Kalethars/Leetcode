# 给定两个以字符串形式表示的非负整数num1和num2，返回num1和num2的乘积，它们的乘积也表示为字符串形式。
# 示例 1:
# 输入: num1 = "2", num2 = "3"
# 输出: "6"
# 示例 2:
# 输入: num1 = "123", num2 = "456"
# 输出: "56088"
# 说明：
# num1和num2的长度小于110。
# num1和num2只包含数字0-9。
# num1和num2均不以零开头，除非是数字 0 本身。
# 不能使用任何标准库的大数类型（比如 BigInteger）或直接将输入转换为整数来处理。

class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        results = [0] * (len(num1) + len(num2))
        num1, num2 = num1[::-1], num2[::-1]
        for p1, n1 in enumerate(num1):
            for p2, n2 in enumerate(num2):
                results[p1 + p2] += (ord(n1) - 48) * (ord(n2) - 48)
        carrier = 0
        for i in range(len(results)):
            value = results[i] + carrier
            carrier = value // 10
            results[i] = value % 10
        return ''.join([str(n) for n in results[::-1]]).lstrip('0') or '0'


if __name__ == '__main__':
    assert Solution().multiply('2', '3') == '6'
    assert Solution().multiply('123', '456') == '56088'
