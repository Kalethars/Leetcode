# 给定一个字符串数组，将字母异位词组合在一起。字母异位词指字母相同，但排列不同的字符串。
# 示例:
# 输入: ['eat', 'tea', 'tan', 'ate', 'nat', 'bat']
# 输出:
# [
#   ['ate','eat','tea'],
#   ['nat','tan'],
#   ['bat']
# ]
# 说明：
# 所有输入均为小写字母。
# 不考虑答案输出的顺序。

from typing import List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        grouped = dict()
        for s in strs:
            counter = dict()
            for c in s:
                counter[c] = counter.get(c, 0) + 1
            feature = ''.join([f'{c}{counter[c]}' for c in 'abcdefghijklmnopqrstuvwxyz' if c in counter])
            grouped[feature] = grouped.get(feature, []) + [s]
        return list(grouped.values())


if __name__ == '__main__':
    assert len(Solution().groupAnagrams(['eat', 'tea', 'tan', 'ate', 'nat', 'bat'])) == 3
