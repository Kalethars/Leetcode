# 实现一个函数，检查一棵二叉树是否为二叉搜索树。
# 示例 1:
# 输入:
#     2
#    / \
#   1   3
# 输出: true
# 示例 2:
# 输入:
#     5
#    / \
#   1   4
#      / \
#     3   6
# 输出: false
# 解释: 输入为: [5,1,4,null,null,3,6]。
#      根节点的值为 5 ，但是其右子节点值为 4 。

from python3.treenode import TreeNode


class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        return self.bst_min_max(root)[0]

    def bst_min_max(self, tree: TreeNode):
        if tree is None:
            return True, None, None
        left_valid, left_min, left_max = self.bst_min_max(tree.left)
        right_valid, right_min, right_max = self.bst_min_max(tree.right)
        return left_valid and right_valid and \
               (left_max is None or tree.val > left_max) and (right_min is None or tree.val < right_min), \
               left_min or tree.val, right_max or tree.val


if __name__ == '__main__':
    assert Solution().isValidBST(TreeNode.build_tree([2, 1, 3])) is True
    assert Solution().isValidBST(TreeNode.build_tree([5, 1, 4, None, None, 3, 6])) is False
    assert Solution().isValidBST(TreeNode.build_tree([10, 5, 15, None, None, 6, 20])) is False
