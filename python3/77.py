# 给定两个整数 n 和 k，返回 1 ... n 中所有可能的 k 个数的组合。
# 示例:
# 输入: n = 4, k = 2
# 输出:
# [
#   [2,4],
#   [3,4],
#   [2,3],
#   [1,2],
#   [1,3],
#   [1,4],
# ]

from typing import List


class Solution:
    def combine(self, n: int, k: int, base=1) -> List[List]:
        return sum([[[i + base] + combination for combination in self.combine(n - i - 1, k - 1, base + i + 1)]
                    for i in range(n - k + 1)], []) if k > 0 else [[]]


if __name__ == '__main__':
    assert len(Solution().combine(4, 2)) == 6
    assert len(Solution().combine(10, 5)) == 252
