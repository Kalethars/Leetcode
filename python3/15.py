# 给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
# 注意：答案中不可以包含重复的三元组。

from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        self.nums = sorted(nums)
        self.remained_nums = sorted(set(self.nums))
        self.counts = dict()
        for num in self.nums:
            self.counts[num] = self.counts.get(num, 0) + 1
        results = []
        for index, num in enumerate(self.nums[:-2]):
            if num > 0:
                break
            if self.counts[num] == 1:
                del self.counts[num]
                self.remained_nums.pop(0)
            else:
                self.counts[num] -= 1
            if index > 0 and num == self.nums[index - 1]:
                continue
            results.extend([result + [num] for result in self.two_sum(index)])
        return results

    def two_sum(self, index):
        target = -self.nums[index]
        results = []
        for num in self.remained_nums:
            if num * 2 > target:
                break
            if self.counts.get(target - num, 0) > (target - num == num):
                results.append([num, target - num])
        return results


if __name__ == '__main__':
    assert Solution().threeSum([-1, 0, 1, 2, -1, -4]) == [[-1, -1, 2], [-1, 0, 1]]
