# 给定一个无重复元素的数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
# candidates 中的数字可以无限制重复被选取。
#
# 说明：
# 所有数字（包括 target）都是正整数。
# 解集不能包含重复的组合。

from typing import List


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        self.candidates = sorted(candidates)
        self.results = dict()
        return self.get_combinations(target, len(self.candidates) - 1)

    def get_combinations(self, target, max_index):
        from bisect import bisect_right
        results, stop_index = [], 0
        if target in self.results:
            if self.results[target][1] >= max_index:
                return self.results[target][0]
            stop_index, results = self.results[target][1] + 1, self.results[target][0]
        for index in range(bisect_right(self.candidates, min(target, self.candidates[max_index])) - 1, stop_index - 1,
                           -1):
            candidate = self.candidates[index]
            if candidate == target:
                results.append([candidate])
            else:
                results.extend([[candidate, *result]
                                for result in self.get_combinations(target - candidate, index)
                                if result[0] <= candidate])
        self.results[target] = results, max_index
        return results


class Solution2:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        self.candidates = sorted(candidates)
        self.results = dict()
        return self.get_combinations(target)

    def get_combinations(self, target):
        stop_index, results = 0, []
        if target in self.results:
            return self.results[target]
        for index, candidate in enumerate(self.candidates):
            if candidate == target:
                results.append([candidate])
            elif candidate > target:
                break
            else:
                results.extend([[candidate, *result]
                                for result in self.get_combinations(target - candidate)
                                if result[0] >= candidate])
        self.results[target] = results
        return results


if __name__ == '__main__':
    assert len(Solution().combinationSum([2, 3, 6, 7], 7)) == 2
    assert len(Solution().combinationSum([2, 3, 5], 8)) == 3
    assert len(Solution2().combinationSum([2, 3, 6, 7], 7)) == 2
    assert len(Solution2().combinationSum([2, 3, 5], 8)) == 3
