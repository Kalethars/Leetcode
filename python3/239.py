# 给你一个整数数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。
# 返回滑动窗口中的最大值。
# 示例 1：
# 输入：nums = [1,3,-1,-3,5,3,6,7], k = 3
# 输出：[3,3,5,5,6,7]
# 解释：
# 滑动窗口的位置                最大值
# ---------------               -----
# [1  3  -1] -3  5  3  6  7       3
#  1 [3  -1  -3] 5  3  6  7       3
#  1  3 [-1  -3  5] 3  6  7       5
#  1  3  -1 [-3  5  3] 6  7       5
#  1  3  -1  -3 [5  3  6] 7       6
#  1  3  -1  -3  5 [3  6  7]      7
# 示例 2：
# 输入：nums = [1], k = 1
# 输出：[1]
# 示例 3：
# 输入：nums = [1,-1], k = 1
# 输出：[1,-1]
# 示例 4：
# 输入：nums = [9,11], k = 2
# 输出：[11]
# 示例 5：
# 输入：nums = [4,-2], k = 2
# 输出：[4]
# 提示：
# 
# 1 <= nums.length <= 10^5
# -10^4 <= nums[i] <= 10^4
# 1 <= k <= nums.length

import bisect
from typing import List


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        max_num_index = nums.index(max(nums[:k]))
        window, pushed = sorted(nums[max_num_index:k]), {i for i in range(max_num_index, k)}
        results = [window[-1]]
        for index in range(len(nums) - k):
            num_pop, num_push = nums[index], nums[index + k]
            if num_push > window[-1]:
                window, pushed = [num_push], {index + k}
            else:
                if index in pushed:
                    window.pop(bisect.bisect_left(window, num_pop))
                bisect.insort(window, num_push)
                pushed.add(index + k)
            results.append(window[-1])
        return results


if __name__ == '__main__':
    assert Solution().maxSlidingWindow([1], 1) == [1]
    assert Solution().maxSlidingWindow([1, -1], 1) == [1, -1]
    assert Solution().maxSlidingWindow([9, 11], 2) == [11]
    assert Solution().maxSlidingWindow([4, -2], 2) == [4]
    assert Solution().maxSlidingWindow([1, 3, 1, -3, 5, 3, 6, 7], 3) == [3, 3, 5, 5, 6, 7]
    assert Solution().maxSlidingWindow([9, 10, 9, -7, -4, -8, 2, -6], 5) == [10, 10, 9, 2]
    assert Solution().maxSlidingWindow([-6, -10, -7, -1, -9, 9, -8, -4, 10, -5, 2, 9, 0, -7, 7, 4, -2, -10, 8, 7], 7) \
           == [9, 9, 10, 10, 10, 10, 10, 10, 10, 9, 9, 9, 8, 8]
