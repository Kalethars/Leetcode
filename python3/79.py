# 给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。
# 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。
# 示例 1：
# 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
# 输出：true
# 示例 2：
# 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
# 输出：true
# 示例 3：
# 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
# 输出：false
# 提示：
# m == board.length
# n = board[i].length
# 1 <= m, n <= 6
# 1 <= word.length <= 15
# board 和 word 仅由大小写英文字母组成

from typing import List


class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        if len(word) == 0:
            return True
        elif len(board) * len(board[0]) < len(word):
            return False
        self.board = board
        for row_index, row in enumerate(board):
            for column_index, char in enumerate(row):
                if char == word[0]:
                    if self.exist_word(row_index, column_index, word[1:]):
                        return True
        return False

    def exist_word(self, row_index, column_index, word):
        if len(word) == 0:
            return True
        char, self.board[row_index][column_index] = self.board[row_index][column_index], ''
        for delta_row, delta_column in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            next_row_index, next_column_index = row_index + delta_row, column_index + delta_column
            if 0 <= next_row_index < len(self.board) and 0 <= next_column_index < len(self.board[0]) \
                    and self.board[next_row_index][next_column_index] == word[0] \
                    and self.exist_word(next_row_index, next_column_index, word[1:]):
                return True
        self.board[row_index][column_index] = char
        return False


if __name__ == '__main__':
    assert Solution().exist([['A', 'B', 'C', 'E'], ['S', 'F', 'C', 'S'], ['A', 'D', 'E', 'E']], 'ABCCED') is True
    assert Solution().exist([['A', 'B', 'C', 'E'], ['S', 'F', 'C', 'S'], ['A', 'D', 'E', 'E']], 'SEE') is True
    assert Solution().exist([['A', 'B', 'C', 'E'], ['S', 'F', 'C', 'S'], ['A', 'D', 'E', 'E']], 'ABCB') is False
