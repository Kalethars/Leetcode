# 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
# 示例 1：
# 输入：nums1 = [1,3], nums2 = [2]
# 输出：2.00000
# 解释：合并数组 = [1,2,3] ，中位数 2
# 示例 2：
# 输入：nums1 = [1,2], nums2 = [3,4]
# 输出：2.50000
# 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
# 示例 3：
# 输入：nums1 = [0,0], nums2 = [0,0]
# 输出：0.00000
# 示例 4：
# 输入：nums1 = [], nums2 = [1]
# 输出：1.00000
# 示例 5：
# 输入：nums1 = [2], nums2 = []
# 输出：2.00000
# 提示：
# nums1.length == m
# nums2.length == n
# 0 <= m <= 1000
# 0 <= n <= 1000
# 1 <= m + n <= 2000
# -10^6 <= nums1[i], nums2[i] <= 10^6

from typing import List
import random


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        total_length = len(nums1) + len(nums2)
        if total_length % 2 == 1:
            return self.find_kth(nums1, nums2, total_length // 2)
        else:
            return (self.find_kth(nums1, nums2, total_length // 2 - 1) +
                    self.find_kth(nums1, nums2, total_length // 2)) / 2

    def find_kth(self, nums1, nums2, k):
        if len(nums1) == 0 or len(nums2) == 0:
            return (nums1 + nums2)[k]
        num = random.choice(nums1 + nums2)
        pos1, pos2 = self.find_position(nums1, num), self.find_position(nums2, num)
        if pos1 + pos2 == k:
            return num
        elif pos1 + pos2 < k:
            return self.find_kth(nums1[pos1:], nums2[pos2:], k - pos1 - pos2)
        else:
            return self.find_kth(nums1[:pos1], nums2[:pos2], k)

    def find_position(self, nums, num):
        if len(nums) == 0:
            return 0
        left, right = 0, len(nums) - 1
        while left < right:
            middle = (left + right) // 2
            if nums[middle] < num:
                left = middle + 1
            else:
                right = middle
        return right + 1 if nums[right] < num else right


if __name__ == '__main__':
    assert Solution().findMedianSortedArrays([1, 2], [3]) == 2
    assert Solution().findMedianSortedArrays([3, 4], [1, 2]) == 2.5
    assert Solution().findMedianSortedArrays([1, 3], [2]) == 2
    assert Solution().findMedianSortedArrays([0, 0], [0, 0]) == 0
    assert Solution().findMedianSortedArrays([], [1]) == 1
    assert Solution().findMedianSortedArrays([2], []) == 2
    assert Solution().findMedianSortedArrays([1, 3], [2, 7]) == 2.5
