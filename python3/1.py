# 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
# 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。

from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        indices = dict()
        for i, n in enumerate(nums):
            if target - n in indices:
                return [indices[target - n], i]
            indices[n] = i


if __name__ == '__main__':
    assert Solution().twoSum([2, 7, 11, 5], 9) == [0, 1]
