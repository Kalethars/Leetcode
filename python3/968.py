# 给定一个二叉树，我们在树的节点上安装摄像头。
# 节点上的每个摄影头都可以监视其父对象、自身及其直接子对象。
# 计算监控树的所有节点所需的最小摄像头数量。
# 示例 1：
# 输入：[0,0,null,0,0]
# 输出：1
# 解释：如图所示，一台摄像头足以监控所有节点。
# 示例 2：
# 输入：[0,0,null,0,null,0,null,null,0]
# 输出：2
# 解释：需要至少两个摄像头来监视树的所有节点。 上图显示了摄像头放置的有效位置之一。
# 提示：
# 给定树的节点数的范围是 [1, 1000]。
# 每个节点的值都是 0。

from python3.treenode import TreeNode


class Solution:
    def minCameraCover(self, root: TreeNode) -> int:
        return min(self.cover(root)[:-1])

    def cover(self, tree: TreeNode):
        if tree is None:
            return 1, 0, 0
        # current node installed, current node covered, all nodes covered except current node
        left_installed, left_covered, left_uncovered = self.cover(tree.left)
        right_installed, right_covered, right_uncovered = self.cover(tree.right)
        return min(left_installed, left_covered, left_uncovered) + \
               min(right_installed, right_covered, right_uncovered) + 1, \
               max(min(left_installed + right_covered, left_covered + right_installed,
                       left_installed + right_installed), 1), \
               left_covered + right_covered


if __name__ == '__main__':
    assert Solution().minCameraCover(TreeNode.build_tree([1])) == 1
    assert Solution().minCameraCover(TreeNode.build_tree([1, 2])) == 1
    assert Solution().minCameraCover(TreeNode.build_tree([1, 2, 3])) == 1
    assert Solution().minCameraCover(TreeNode.build_tree([1, 2, None, 3])) == 1
    assert Solution().minCameraCover(TreeNode.build_tree([1, 2, None, 3, 4])) == 1
    assert Solution().minCameraCover(TreeNode.build_tree([1, 2, 3, 4, 5, 6, 7])) == 2
    assert Solution().minCameraCover(
        TreeNode.build_tree([1, 2, None, 3, None, None, None, 4, None, None, None, None, None, None, None, None, 5])
    ) == 2
