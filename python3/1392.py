class Solution:
    def longestPrefix(self, s: str) -> str:
        for index in range(len(s) - 2, -1, -1):
            if s[index] == s[-1] and s[0] == s[len(s) - index - 1] and s[:index + 1] == s[len(s) - index - 1:]:
                return s[:index + 1]
        return ''


if __name__ == '__main__':
    assert Solution().longestPrefix('level') == 'l'
    assert Solution().longestPrefix('ab' * 50000) == 'ab' * 49999
