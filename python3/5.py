# 给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。

class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s) == 0:
            return ''
        result = ''
        for i in range(len(s)):
            length = 3
            while i - length // 2 >= 0 and i + length // 2 < len(s):
                if s[i - length // 2] != s[i + length // 2]:
                    break
                length += 2
            if length - 2 > len(result):
                result = s[i - length // 2 + 1:i + length // 2]
        for i in range(len(s) - 1):
            length = 2
            while i - length // 2 + 1 >= 0 and i + length // 2 < len(s):
                if s[i - length // 2 + 1] != s[i + length // 2]:
                    break
                length += 2
            if length - 2 > len(result):
                result = s[i - length // 2 + 2:i + length // 2]
        return result


if __name__ == '__main__':
    assert len(Solution().longestPalindrome('babad')) == 3
    assert len(Solution().longestPalindrome('cbbd')) == 2
