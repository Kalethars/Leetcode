# 给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。
# 示例 1：
# 输入：head = [1,2,3,4,5], k = 2
# 输出：[4,5,1,2,3]
# 示例 2：
# 输入：head = [0,1,2], k = 4
# 输出：[2,0,1]
# 提示：
# 链表中节点的数目在范围 [0, 500] 内
# -100 <= Node.val <= 100
# 0 <= k <= 2 * 109
from python3.listnode import ListNode


class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        if head is None or head.next is None:
            return head
        node = head
        length = 0
        while node is not None:
            length += 1
            node = node.next
        step = length - k % length - 1
        if step == length - 1:
            return head
        node = head
        for i in range(step):
            node = node.next
        node.next, node = None, node.next
        new_head = node
        while node.next is not None:
            node = node.next
        node.next = head
        return new_head


if __name__ == '__main__':
    assert Solution().rotateRight(ListNode.build_chain(1, 2, 3, 4, 5), 2).get_chain() == [4, 5, 1, 2, 3]
    assert Solution().rotateRight(ListNode.build_chain(0, 1, 2), 4).get_chain() == [2, 0, 1]
    assert Solution().rotateRight(ListNode.build_chain(1, 2), 1).get_chain() == [2, 1]
