# 实现pow(x, n)，即计算 x 的 n 次幂函数（即，x^n）。
# 示例 1：
# 输入：x = 2.00000, n = 10
# 输出：1024.00000
# 示例 2：
# 输入：x = 2.10000, n = 3
# 输出：9.26100
# 示例 3：
# 输入：x = 2.00000, n = -2
# 输出：0.25000
# 解释：2-2 = 1/22 = 1/4 = 0.25
# 提示：
# -100.0 <x< 100.0
# -2^31<= n <=2^31-1
# -10^4 <= x^n <= 10^4

class Solution:
    def myPow(self, x: float, n: int) -> float:
        return [1, x, 1 / x][n] if n in [0, 1, -1] else self.myPow(x, n // 2) ** 2 * self.myPow(x, n % 2)


if __name__ == '__main__':
    assert abs(Solution().myPow(2, 10) - 1024) < 1e-5
    assert abs(Solution().myPow(2.1, 3) - 9.261) < 1e-5
    assert abs(Solution().myPow(2, -2) - 0.25) < 1e-5
