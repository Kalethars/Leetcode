# 给定一个只包含 '(' 和 ')' 的字符串，找出最长的包含有效括号的子串的长度。

class Solution:
    def longestValidParentheses(self, s: str) -> int:
        self.length = 0
        self.find_longest(s)
        return self.length

    def find_longest(self, s):
        if len(s) < self.length:
            return
        match_head = s.find('()')
        if match_head == -1:
            return
        match_tail = match_head + 1
        difference = 0
        for right_search_index in range(match_tail + 1, len(s)):
            difference += 1 if s[right_search_index] == '(' else -1
            if difference == 0:
                match_tail = right_search_index
            elif difference == -1:
                if match_head > 0 and s[match_head - 1] == '(':
                    match_head, match_tail = match_head - 1, right_search_index
                    difference = 0
                else:
                    break
            elif difference > len(s) - right_search_index - 1:
                break
        self.length = max(self.length, match_tail - match_head + 1)
        self.find_longest(s[match_tail + 1:])


if __name__ == '__main__':
    assert Solution().longestValidParentheses('(()') == 2
    assert Solution().longestValidParentheses(')()())') == 4
