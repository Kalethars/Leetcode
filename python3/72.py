# 给你两个单词 word1 和 word2，请你计算出将 word1 转换成 word2 所使用的最少操作数 。
# 你可以对一个单词进行如下三种操作：
# 插入一个字符
# 删除一个字符
# 替换一个字符
# 示例 1：
# 输入：word1 = "horse", word2 = "ros"
# 输出：3
# 解释：
# horse -> rorse (将 'h' 替换为 'r')
# rorse -> rose (删除 'r')
# rose -> ros (删除 'e')
# 示例 2：
# 输入：word1 = "intention", word2 = "execution"
# 输出：5
# 解释：
# intention -> inention (删除 't')
# inention -> enention (将 'i' 替换为 'e')
# enention -> exention (将 'n' 替换为 'x')
# exention -> exection (将 'n' 替换为 'c')
# exection -> execution (插入 'u')
# 提示：
# 0 <= word1.length, word2.length <= 500
# word1 和 word2 由小写英文字母组成

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        for index, (char1, char2) in enumerate(zip(word1, word2)):
            if char1 != char2:
                word1, word2 = word1[index:], word2[index:]
                break
        for index, (char1, char2) in enumerate(zip(word1[::-1], word2[::-1])):
            if char1 != char2:
                word1, word2 = word1[:len(word1) - index + 1], word2[:len(word2) - index + 1]
                break
        if len(word1) == 0 or len(word2) == 0:
            return max(len(word1), len(word2))
        distances = [[i + j if i * j == 0 else -1 for j in range(len(word2) + 1)] for i in range(len(word1) + 1)]
        for i, char1 in enumerate(word1):
            for j, char2 in enumerate(word2):
                distances[i + 1][j + 1] = min(distances[i][j + 1], distances[i + 1][j],
                                              distances[i][j] - (char1 == char2)) + 1
        return distances[-1][-1]


if __name__ == '__main__':
    assert Solution().minDistance('horse', 'ros') == 3
    assert Solution().minDistance('intention', 'execution') == 5
