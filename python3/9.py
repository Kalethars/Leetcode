# 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。

class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        if x == 0:
            return True
        import math
        length = int(math.log(x, 10)) + 1
        num = 0
        for i in range(length):
            if i < length // 2:
                num = num * 10 + x % 10
            elif i > (length - 1) // 2:
                if num % 10 != x % 10:
                    return False
                else:
                    num //= 10
            x //= 10
        return True


if __name__ == '__main__':
    assert Solution().isPalindrome(121) is True
    assert Solution().isPalindrome(-121) is False
    assert Solution().isPalindrome(10) is False
