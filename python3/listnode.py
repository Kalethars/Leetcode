class ListNode:
    def __init__(self, x, next=None):
        self.val = x
        self.next = next

    @classmethod
    def build_chain(cls, *vals):
        return None if len(vals) == 0 else ListNode(vals[0], next=cls.build_chain(*vals[1:]))

    def get_chain(self):
        vals = [self.val]
        head = self
        while head.next is not None:
            head = head.next
            vals.append(head.val)
        return vals
