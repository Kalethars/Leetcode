# 给定一个非负整数数组，你最初位于数组的第一个位置。
# 数组中的每个元素代表你在该位置可以跳跃的最大长度。
# 你的目标是使用最少的跳跃次数到达数组的最后一个位置。
# 假设你总是可以到达数组的最后一个位置。
# 示例 1:
# 输入: [2,3,1,1,4]
# 输出: 2
# 解释: 跳到最后一个位置的最小跳跃数是 2。
#     从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
# 示例 2:
# 输入: [2,3,0,1,4]
# 输出: 2
#
# 提示:
# 1 <= nums.length <= 10000
# 0 <= nums[i] <= 10^5

from typing import List


class Solution:
    def jump(self, nums: List[int]) -> int:
        distances = [-1, 0]  # distances[i] = farthest distance in i - 1 steps
        while distances[-1] < len(nums) - 1:
            distances.append(max(i + nums[i] for i in range(distances[-1], distances[-2], -1)))
        return len(distances) - 2

    def jump_n2(self, nums: List[int]) -> int:
        jumps = [0]
        for i, length in enumerate(nums[1:]):
            jumps.append(min(jumps[j] + 1 for j in range(i + 1) if i - j + 1 <= nums[j]))
        return jumps[-1]


if __name__ == '__main__':
    assert Solution().jump([0]) == 0
    assert Solution().jump([2, 3, 1, 1, 4]) == 2
    assert Solution().jump([2, 3, 0, 1, 4]) == 2
