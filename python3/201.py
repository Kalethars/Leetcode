# 给定范围 [m, n]，其中 0 <= m <= n <= 2147483647，返回此范围内所有数字的按位与（包含 m, n 两端点）。

class Solution:
    def rangeBitwiseAnd(self, m: int, n: int) -> int:
        bin_m, bin_n = bin(m)[2:], bin(n)[2:]
        if len(bin_m) != len(bin_n):
            return 0
        result = ''
        for bit_m, bit_n in zip(bin_m, bin_n):
            if bit_m == bit_n:
                result += bit_m
            else:
                break
        result += '0' * (len(bin_m) - len(result))
        return int(result, 2)


if __name__ == '__main__':
    assert Solution().rangeBitwiseAnd(5, 7) == 4
    assert Solution().rangeBitwiseAnd(0, 1) == 0
