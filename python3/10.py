# 给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。
# '.' 匹配任意单个字符
# '*' 匹配零个或多个前面的那一个元素
# 所谓匹配，是要涵盖 整个 字符串 s的，而不是部分字符串。
#
# 说明:
# s 可能为空，且只包含从 a-z 的小写字母。
# p 可能为空，且只包含从 a-z 的小写字母，以及字符 . 和 *。

class Solution:
    def __init__(self):
        self.record = dict()

    def isMatch(self, s: str, p: str) -> bool:
        if (s, p) in self.record:
            return self.record[(s, p)]
        if len(s) == 0 and len(p) == 0:
            return self.save_status(s, p, True)
        elif len(p) == 0 or len(s) == 0 and p[-1] != '*':
            return self.save_status(s, p, False)
        if p[-1] == '.':
            return self.isMatch(s[:-1], p[:-1])
        if p[-1] == '*':
            if self.isMatch(s, p[:-2]):
                return self.save_status(s, p, True)
            elif len(s) > 0 and (p[-2] == '.' or s[-1] == p[-2]):
                return self.isMatch(s[:-1], p)
            else:
                return self.save_status(s, p, False)
        if p[-1] != s[-1]:
            return self.save_status(s, p, False)
        else:
            return self.isMatch(s[:-1], p[:-1])

    def save_status(self, s, p, status):
        self.record[(s, p)] = status
        return status


if __name__ == '__main__':
    assert Solution().isMatch('aa', 'a') is False
    assert Solution().isMatch('aa', 'a*') is True
    assert Solution().isMatch('ab', '.*') is True
    assert Solution().isMatch('aab', 'c*a*b') is True
    assert Solution().isMatch('mississippi', 'mis*is*p*.') is False
