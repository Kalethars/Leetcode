# 设计一个算法，找出二叉搜索树中指定节点的“下一个”节点（也即中序后继）。
# 如果指定节点没有对应的“下一个”节点，则返回null。
# 示例 1:
# 输入: root = [2,1,3], p = 1
#   2
#  / \
# 1   3
# 输出: 2
# 示例 2:
# 输入: root = [5,3,6,2,4,null,null,1], p = 6
#
#       5
#      / \
#     3   6
#    / \
#   2   4
#  /
# 1
#
# 输出: null

from python3.treenode import TreeNode


class Solution:
    def inorderSuccessor(self, root: TreeNode, p: TreeNode) -> TreeNode:
        ldr = self.ldr(root)
        index = ldr.index(p)
        return None if index == len(ldr) - 1 else ldr[index + 1]

    @classmethod
    def ldr(cls, tree: TreeNode):
        return (cls.ldr(tree.left) if tree.left else []) + [tree] + (cls.ldr(tree.right) if tree.right else [])


if __name__ == '__main__':
    tree = TreeNode.build_tree([2, 1, 3])
    assert Solution().inorderSuccessor(tree, tree.left) == tree

    tree = TreeNode.build_tree([5, 3, 6, 2, 4, None, None, 1])
    assert Solution().inorderSuccessor(tree, tree.right) is None
