# 给定一个包含 n 个整数的数组 nums 和一个目标值 target，判断 nums 中是否存在四个元素 a，b，c 和 d ，使得 a + b + c + d 的值与 target 相等？找出所有满足条件且不重复的四元组。
# 注意：答案中不可以包含重复的四元组。

from typing import List


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        nums.sort()
        sum_before, sum_after = {i: dict() for i in range(len(nums))}, {i: dict() for i in range(len(nums))}
        for i in range(len(nums) - 1):
            for j in range(i + 1, len(nums)):
                self.add_to_set(sum_before[j], nums[i] + nums[j], (nums[i], nums[j]))
                self.add_to_set(sum_after[i], nums[i] + nums[j], (nums[i], nums[j]))
        results = set()
        for i in range(1, len(nums) - 2):
            for j in range(i + 1, len(nums) - 1):
                for key in sum_before[i].keys():
                    if target - key in sum_after[j]:
                        for v1 in sum_before[i][key]:
                            for v2 in sum_after[j][target - key]:
                                results.add((*v1, *v2))
        return [list(result) for result in results]

    def add_to_set(self, dic, key, value):
        if key not in dic:
            dic[key] = set()
        dic[key].add(value)


class Solution2:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        nums.sort()
        results = []
        for i, num in enumerate(nums[:-3]):
            if i > 0 and num == nums[i - 1]:
                continue
            results.extend([result + [num] for result in self.three_sum(nums[i + 1:], target - num)])
        return results

    def three_sum(self, nums: List[int], target: int) -> List[List[int]]:
        self.nums = nums
        self.remained_nums = sorted(set(self.nums))
        self.counts = dict()
        for num in self.nums:
            self.counts[num] = self.counts.get(num, 0) + 1
        results = []
        for index, num in enumerate(self.nums[:-2]):
            if num * 3 > target:
                break
            if self.counts[num] == 1:
                del self.counts[num]
                self.remained_nums.pop(0)
            else:
                self.counts[num] -= 1
            if index > 0 and num == self.nums[index - 1]:
                continue
            results.extend([result + [num] for result in self.two_sum(target - self.nums[index])])
        return results

    def two_sum(self, target):
        results = []
        for num in self.remained_nums:
            if num * 2 > target:
                break
            if self.counts.get(target - num, 0) > (target - num == num):
                results.append([num, target - num])
        return results


if __name__ == '__main__':
    print(Solution().fourSum([1, 0, -1, 0, -2, 2], 0))
    print(Solution2().fourSum([1, 0, -1, 0, -2, 2], 0))
