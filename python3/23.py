# 合并 k 个排序链表，返回合并后的排序链表。请分析和描述算法的复杂度。

from typing import List

from python3.listnode import ListNode


class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        from queue import PriorityQueue
        queue = PriorityQueue()
        for index, node in enumerate(lists):
            if node is not None:
                queue.put((node.val, index))
        head = node = ListNode(None)
        while not queue.empty():
            value, index = queue.get()
            node.next = ListNode(value)
            if lists[index].next is not None:
                lists[index] = lists[index].next
                queue.put((lists[index].val, index))
            node = node.next
        return head.next


class Solution2:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        values = []
        for node in lists:
            while node is not None:
                values.append(node.val)
                node = node.next
        head = node = ListNode(None)
        for value in sorted(values):
            node.next = ListNode(value)
            node = node.next
        return head.next


if __name__ == '__main__':
    assert Solution().mergeKLists(
        [ListNode.build_chain(1, 4, 5),
         ListNode.build_chain(1, 3, 4),
         ListNode.build_chain(2, 6)]
    ).get_chain() == [1, 1, 2, 3, 4, 4, 5, 6]
    assert Solution2().mergeKLists(
        [ListNode.build_chain(1, 4, 5),
         ListNode.build_chain(1, 3, 4),
         ListNode.build_chain(2, 6)]
    ).get_chain() == [1, 1, 2, 3, 4, 4, 5, 6]
