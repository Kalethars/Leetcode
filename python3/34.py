# 给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。
# 你的算法时间复杂度必须是 O(log n) 级别。
# 如果数组中不存在目标值，返回 [-1, -1]。

from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if len(nums) == 0 or nums[0] > target or nums[-1] < target:
            return [-1, -1]
        if len(nums) == 1:
            return [0, 0] if nums[0] == target else [-1, -1]
        mid = len(nums) // 2
        if nums[mid] > target:
            return self.searchRange(nums[:mid], target)
        elif nums[mid] < target:
            indices = self.searchRange(nums[mid + 1:], target)
            return indices if indices == [-1, -1] else [mid + 1 + indices[0], mid + 1 + indices[1]]
        else:
            left_indices = self.searchRange(nums[:mid], target)
            right_indices = self.searchRange(nums[mid + 1:], target)
            left_index = mid if left_indices == [-1, -1] else left_indices[0]
            right_index = mid if right_indices == [-1, -1] else mid + 1 + right_indices[1]
            return [left_index, right_index]


if __name__ == '__main__':
    assert Solution().searchRange([5, 7, 7, 8, 8, 10], 8) == [3, 4]
    assert Solution().searchRange([5, 7, 7, 8, 8, 10], 6) == [-1, -1]
