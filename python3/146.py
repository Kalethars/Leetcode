# 运用你所掌握的数据结构，设计和实现一个  LRU (最近最少使用) 缓存机制 。
# 实现 LRUCache 类：
# LRUCache(int capacity) 以正整数作为容量 capacity 初始化 LRU 缓存
# int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。
# void put(int key, int value) 如果关键字已经存在，则变更其数据值；如果关键字不存在，则插入该组「关键字-值」。当缓存容量达到上限时，它应该在写入新数据之前删除最久未使用的数据值，从而为新的数据值留出空间。
# 进阶：你是否可以在 O(1) 时间复杂度内完成这两种操作？
# 示例：
# 输入
# ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
# [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
# 输出
# [null, null, null, 1, null, -1, null, -1, 3, 4]
# 解释
# LRUCache lRUCache = new LRUCache(2);
# lRUCache.put(1, 1); // 缓存是 {1=1}
# lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
# lRUCache.get(1);    // 返回 1
# lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
# lRUCache.get(2);    // 返回 -1 (未找到)
# lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
# lRUCache.get(1);    // 返回 -1 (未找到)
# lRUCache.get(3);    // 返回 3
# lRUCache.get(4);    // 返回 4
# 提示：
# 1 <= capacity <= 3000
# 0 <= key <= 10000
# 0 <= value <= 10^5
# 最多调用 2 * 10^5 次 get 和 put

from typing import Dict


class LRUCache:
    class CacheInstance:
        key: int
        value: int
        previous: 'LRUCache.CacheInstance'
        next: 'LRUCache.CacheInstance'

        def __init__(self, **kwargs):
            for k, v in kwargs.items():
                setattr(self, k, v)
            if 'value' in kwargs and kwargs['value'] is None and hasattr(self, 'key'):
                self.value = self.key

    def __init__(self, capacity: int):
        self.capacity = capacity
        self.cache: Dict[int, LRUCache.CacheInstance] = dict()
        self.first_instance, self.last_instance = None, None

    def get(self, key: int) -> int:
        if key not in self.cache:
            return -1
        instance = self.cache[key]
        if self.last_instance != instance:
            if instance.previous is not None:
                instance.previous.next = instance.next
            if instance.next is not None:
                instance.next.previous = instance.previous
                if instance.previous is None:
                    self.first_instance = instance.next
            self.last_instance.next = instance
            instance.previous, instance.next = self.last_instance, None
            self.last_instance = instance
        return instance.value

    def put(self, key: int, value: int) -> None:
        if key in self.cache:
            self.cache[key].value = key if value is None else value
            self.get(key)
            return
        self.cache[key] = self.CacheInstance(key=key, value=value, previous=self.last_instance, next=None)
        if self.first_instance is None:
            self.first_instance = self.cache[key]
        if self.last_instance is not None:
            self.last_instance.next = self.cache[key]
        self.last_instance = self.cache[key]
        if len(self.cache) > self.capacity:
            self.first_instance.next.previous = None
            del self.cache[self.first_instance.key]
            self.first_instance = self.first_instance.next


if __name__ == '__main__':
    lru_cache = LRUCache(2)
    lru_cache.put(1, 1)
    lru_cache.put(2, 2)
    assert lru_cache.get(1) == 1
    lru_cache.put(3, 3)
    assert lru_cache.get(2) == -1
    lru_cache.put(4, 4)
    assert lru_cache.get(1) == -1
    assert lru_cache.get(3) == 3
    assert lru_cache.get(4) == 4

    lru_cache = LRUCache(1)
    lru_cache.put(2, 1)
    assert lru_cache.get(2) == 1

    lru_cache = LRUCache(2)
    lru_cache.put(2, 1)
    lru_cache.put(2, 2)
    assert lru_cache.get(2) == 2
    lru_cache.put(1, 1)
    lru_cache.put(4, 1)
    assert lru_cache.get(2) == -1

    lru_cache = LRUCache(2)
    lru_cache.put(2, 1)
    lru_cache.put(3, 2)
    assert lru_cache.get(3) == 2
    assert lru_cache.get(2) == 1
    lru_cache.put(4, 3)
    assert lru_cache.get(2) == 1
    assert lru_cache.get(3) == -1
    assert lru_cache.get(4) == 3
