# 编写一个程序，通过已填充的空格来解决数独问题。
# 一个数独的解法需遵循如下规则：
# 1. 数字 1-9 在每一行只能出现一次。
# 2. 数字 1-9 在每一列只能出现一次。
# 3. 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。
# 空白格用 '.' 表示。

from typing import List


class Solution:
    def __init__(self, **kwargs):
        from copy import deepcopy
        for key, value in kwargs.items():
            setattr(self, key, deepcopy(value))

    def solveSudoku(self, board: List[List[str]]) -> None:
        self.board = board
        self.choices = [[{i for i in range(1, 10)} for _ in range(9)] for _ in range(9)]
        self.undetermined = {(row, column) for column in range(9) for row in range(9)}
        for row, line in enumerate(board):
            for column, num in enumerate(line):
                if num != '.':
                    self.determine(row, column, int(num))
        self.solve()

    def solve(self):
        undetermined_count = 81
        while undetermined_count != len(self.undetermined):
            undetermined_count = len(self.undetermined)
            for row, column in list(self.undetermined):
                if len(self.choices[row][column]) == 0:
                    return False
                if len(self.choices[row][column]) == 1:
                    self.determine(row, column, next(iter(self.choices[row][column])))
        if len(self.undetermined) == 0:
            return True
        confusing_row, confusing_column = min(self.undetermined, key=lambda pair: len(self.choices[pair[0]][pair[1]]))
        for choice in self.choices[confusing_row][confusing_column]:
            solution = Solution(board=self.board, choices=self.choices, undetermined=self.undetermined)
            solution.determine(confusing_row, confusing_column, choice)
            if solution.solve():
                for row, column in self.undetermined:
                    self.board[row][column] = solution.board[row][column]
                return True
        return False

    def determine(self, row, column, num):
        for i in range(9):
            self.choices[row][i] -= {num}
            self.choices[i][column] -= {num}
            self.choices[row // 3 * 3 + i // 3][column // 3 * 3 + i % 3] -= {num}
        self.board[row][column] = str(num)
        self.choices[row][column] = {num}
        self.undetermined -= {(row, column)}


if __name__ == '__main__':
    sudoku = [
        ['5', '3', '.', '.', '7', '.', '.', '.', '.'],
        ['6', '.', '.', '1', '9', '5', '.', '.', '.'],
        ['.', '9', '8', '.', '.', '.', '.', '6', '.'],
        ['8', '.', '.', '.', '6', '.', '.', '.', '3'],
        ['4', '.', '.', '8', '.', '3', '.', '.', '1'],
        ['7', '.', '.', '.', '2', '.', '.', '.', '6'],
        ['.', '6', '.', '.', '.', '.', '2', '8', '.'],
        ['.', '.', '.', '4', '1', '9', '.', '.', '5'],
        ['.', '.', '.', '.', '8', '.', '.', '7', '9']
    ]
    Solution().solveSudoku(sudoku)
    print('\n\n'.join(
        ['\n'.join([' '.join([''.join(sudoku[i * 3 + j][k * 3:k * 3 + 3]) for k in range(3)]) for j in range(3)])
         for i in range(3)]
    ))
