# 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
#
# 有效字符串需满足：
# 1. 左括号必须用相同类型的右括号闭合。
# 2. 左括号必须以正确的顺序闭合。
# 注意空字符串可被认为是有效字符串。

class Solution:
    def isValid(self, s: str) -> bool:
        parentheses = {'(': ')', '[': ']', '{': '}'}
        if len(s) % 2 != 0:
            return False
        stack = []
        for c in s:
            if c in parentheses:
                stack.append(c)
            else:
                if len(stack) == 0 or parentheses[stack[-1]] != c:
                    return False
                else:
                    stack.pop()
        return len(stack) == 0


if __name__ == '__main__':
    assert Solution().isValid('()') is True
    assert Solution().isValid('()[]{}') is True
    assert Solution().isValid('(]') is False
    assert Solution().isValid('([)]') is False
    assert Solution().isValid('{[]}') is True
