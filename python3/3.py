# 给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        left, length, indices = 0, 0, dict()
        for i, c in enumerate(s):
            if indices.get(c, -1) >= left:
                length = max(length, i - left)
                left = indices[c] + 1
            indices[c] = i
        return max(length, len(s) - left)


if __name__ == '__main__':
    assert Solution().lengthOfLongestSubstring('abcabcbb') == 3
    assert Solution().lengthOfLongestSubstring('bbbbb') == 1
    assert Solution().lengthOfLongestSubstring('pwwkew') == 3
