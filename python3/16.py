# 给定一个包括 n 个整数的数组 nums 和 一个目标值 target。找出 nums 中的三个整数，使得它们的和与 target 最接近。返回这三个数的和。假定每组输入只存在唯一答案。
# 示例：
# 输入：nums = [-1,2,1,-4], target = 1
# 输出：2
# 解释：与 target 最接近的和是 2 (-1 + 2 + 1 = 2) 。
# 提示：
# 3 <= nums.length <= 10^3
# -10^3 <= nums[i] <= 10^3
# -10^4 <= target <= 10^4

from typing import List


class Solution:
    # 双指针
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        nums.sort()
        result = nums[0] + nums[1] + nums[2]
        for i in range(len(nums) - 2):
            left, right = i + 1, len(nums) - 1
            while left < right:
                three_sum = nums[i] + nums[left] + nums[right]
                if abs(three_sum - target) < abs(result - target):
                    result = three_sum
                if three_sum == target:
                    return target
                elif three_sum < target:
                    left += 1
                else:
                    right -= 1
        return result

    def threeSumClosest_n2logn(self, nums: List[int], target: int) -> int:
        self.nums = nums
        self.nums.sort()
        result = self.nums[0] + self.nums[1] + self.nums[2]
        for i in range(len(self.nums) - 2):
            for j in range(i + 1, len(self.nums) - 1):
                if self.nums[i] + self.nums[j] + self.nums[j + 1] - target > abs(result - target):
                    break
                third = self.find_nearest_value(target - self.nums[i] - self.nums[j], j + 1)
                if abs(self.nums[i] + self.nums[j] + third - target) < abs(result - target):
                    result = self.nums[i] + self.nums[j] + third
        return result

    def find_nearest_value(self, target, start):
        left, right = start, len(self.nums) - 1
        if target <= self.nums[left]:
            return self.nums[left]
        if target >= self.nums[right]:
            return self.nums[right]
        while left < right:
            middle = (left + right + 1) // 2
            if target == self.nums[middle]:
                return target
            elif target > self.nums[middle]:
                left = middle
            else:
                right = middle - 1
        best = -1
        for delta in range(-1, 2):
            if start <= left + delta < len(self.nums) and \
                    (best < 0 or abs(self.nums[left + delta] - target) < abs(best - target)):
                best = self.nums[left + delta]
        return best


if __name__ == '__main__':
    assert Solution().threeSumClosest([-1, 2, 1, -4], 1) == 2
    assert Solution().threeSumClosest([1, 2, 4, 8, 16, 32, 64, 128], 82) == 82
    assert Solution().threeSumClosest([1, 1, 48, 50, 99, 100, 103, 333, 333], 250) == 250
