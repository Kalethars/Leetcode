# 给定一个正整数 n（1 ≤ n ≤ 30），输出外观数列的第 n 项。
# 注意：整数序列中的每一项将表示为一个字符串。
# 「外观数列」是一个整数序列，从数字 1 开始，序列中的每一项都是对前一项的描述。前五项如下：
# 1.     1
# 2.     11
# 3.     21
# 4.     1211
# 5.     111221
# 第一项是数字 1
# 描述前一项，这个数是 1 即 “一个 1 ”，记作 11
# 描述前一项，这个数是 11 即 “两个 1 ” ，记作 21
# 描述前一项，这个数是 21 即 “一个 2 一个 1 ” ，记作 1211
# 描述前一项，这个数是 1211 即 “一个 1 一个 2 两个 1 ” ，记作 111221

class Solution:
    def countAndSay(self, n: int) -> str:
        if 'results' not in globals():
            globals()['results'] = ['1']
        results = globals()['results']
        for _ in range(len(results), n):
            description, successor, count = '', results[-1][0], 1
            for char in results[-1][1:] + '$':
                if char != successor:
                    description += f'{count}{successor}'
                    successor, count = char, 1
                else:
                    count += 1
            results.append(description)
        return results[n - 1]


if __name__ == '__main__':
    assert Solution().countAndSay(1) == '1'
    assert Solution().countAndSay(4) == '1211'
