# 给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。
# 示例 1：
# 输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
# 输出：6
# 解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
# 示例 2：
# 输入：height = [4,2,0,3,2,5]
# 输出：9
# 提示：
# n == height.length
# 0 <= n <= 3 * 104
# 0 <= height[i] <= 105

from typing import List


class Solution:
    def trap(self, height: List[int]) -> int:
        if len(height) == 0:
            return 0
        peak_index = max(range(len(height)), key=lambda index: height[index])
        return self.volume(height[:peak_index + 1]) + self.volume(height[peak_index:][::-1])

    def volume(self, heights: List[int]) -> int:
        volume = 0
        wall = heights[0]
        for height in heights[1:]:
            volume += max(wall - height, 0)
            wall = max(wall, height)
        return volume


if __name__ == '__main__':
    assert Solution().trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]) == 6
    assert Solution().trap([4, 2, 0, 3, 2, 5]) == 9
