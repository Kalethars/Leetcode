#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

string AddZero(string s,int a=1)
{
	if (a==0) return s;
	return AddZero(s,a-1)+'0';
}

string Str(int a)
{
string u;
	if (a==0) return "0";
	while (a>0)
	{
		u=char(a%10+48)+u;
		a=a/10;
	}
	return u;
}

string Abs(string s)
{
int i;
string u;
	if (s[0]=='-')
		for (i=1;i<s.length();i++) u=u+s[i];
	else return s;
	return u;
}

string Neg(string s)
{
	if (s[0]=='-') return Abs(s);
	return '-'+s;
}

int comp(string s,string t)
{
int i;
string u,v;
	u=Abs(s);
	v=Abs(t);
	if (u.length()<v.length())
		return 1;
	else if (u.length()>v.length())
		return 0;
	for (i=0;i<u.length();i++)
	{
		if (u[i]<v[i]) return 1;
		else if (u[i]>v[i]) return 0;
	}
	return 2;
}

string posPlus(string s,string t)
{
int i,d,p=0,q=0;
bool f;
string w,u,v;
	if (comp(s,t)==1)
	{
		v=s;s=t;t=v;
	}
	if ((s=="0")&&(t=="0")) return "0";
	s='0'+s;
	d=s.length()-t.length();
	for (i=0;i<d;i++)
		t='0'+t;
	for (i=s.length()-1;i>=0;i--)
	{
		p=(s[i]+t[i]-96+q)/10;
		w=char(s[i]+t[i]-48-p*10+q)+w;
		q=p;
	}
	f=false;
	for (i=0;i<s.length();i++)
	{
		if (w[i]!='0') {
			f=true;
			u=u+w[i];
		}
		if ((w[i]=='0')&&(f)) u=u+w[i];
	}
	return u;
}

string absMinus(string s,string t)
{
int i,d,p=0,q=0;
bool f;
string w,u,v;
	if (comp(s,t)==1)
	{
		v=s;s=t;t=v;
	}
	if (comp(s,t)==2) return "0";
	s='0'+s;
	d=s.length()-t.length();
	for (i=0;i<d;i++)
		t='0'+t;
	for (i=s.length()-1;i>=0;i--)
	{
		p=(s[i]-t[i]+q<0)?-1:0;
		w=char(s[i]-t[i]+48-p*10+q)+w;
		q=p;
	}
	f=false;
	for (i=0;i<s.length();i++)
	{
		if (w[i]!='0') {
			f=true;
			u=u+w[i];
		}
		if ((w[i]=='0')&&(f)) u=u+w[i];
	}
	return u;
}

string Plus(string s,string t)
{
	if ((s[0]=='-')&&(t[0]=='-')) return '-'+posPlus(Abs(s),Abs(t));
	if ((s[0]=='-')&&(comp(s,t)==0)) return '-'+absMinus(Abs(s),t);
	if (s[0]=='-') return absMinus(Abs(s),t);
	if ((t[0]=='-')&&(comp(s,t)==1)) return '-'+absMinus(s,Abs(t));
	if (t[0]=='-') return absMinus(s,Abs(t));
	return posPlus(s,t);
}

string Minus(string s,string t)
{
	return Plus(s,Neg(t));
}

string Round(string s)
{
int i,j;
bool f;
string w,u,p;
	if ((s=="Divided by 0!")||(s=="Invalid!")) return s;
	if (s[s.length()-1]=='i') return "Imaginary!";
	p=Abs(s);
	f=false;
	for (i=0;i<p.length();i++)
		if (p[i]=='e')
		{
			f=true;
			break;
		}
	if (f)
		if ((p[i+1]=='-')&&(p[i+2]=='1'))
			{
				w=p[0];
				if (comp(w,"5")==1) return "0";
				else u="1";
			}
		else return "0";
	else
	{
		f=false;
		for (i=0;i<p.length();i++)
			if (p[i]=='.')
			{
				f=true;
				break;
			}
		if (f)
		{
			for (j=0;j<i;j++) u=u+p[j];
			w=p[i+1];
			if (comp(w,"5")!=1) u=posPlus(u,"1");
		}
		else u=p;
	}
	if (s[0]=='-') return '-'+u;
	return u;
}

int Digit(string s)
{
	return Abs(Round(s)).length();
}

string SinMul(string s,int a)
{
int i,p=0,q=0;
bool f;
string u;
	if (a==0) return "0";
	s='0'+s;
	for (i=s.length()-1;i>=1;i--)
	{
		p=((int(s[i])-48)*a+q)/10;
		s[i]=char((int(s[i])-48)*a+q-p*10+48);
		q=p;
	}
	s[0]=char(q+48);
	f=false;
	for (i=0;i<s.length();i++)
	{
		if (s[i]!='0') {
			f=true;
			u=u+s[i];
		}
		if ((s[i]=='0')&&(f)) u=u+s[i];
	}
	return u;
}

string Multi(string s,string t)
{
int i,j,d;
string w,u,v,p,q;
	p=Abs(s);
	q=Abs(t);
	if (comp(p,q)==1)
	{
		v=p;p=q;q=v;
	}
	if ((p=="0")||(q=="0")) return "0";
	u='0';
	for (i=q.length()-1;i>=0;i--)
	{
		v=AddZero(SinMul(p,int(q[i])-48),q.length()-i-1);
		if (comp(u,v)==1)
		{
			w=u;u=v;v=w;
		}
		u=posPlus(u,v);
	}
	if (((s[0]=='-')&&(t[0]=='-'))||((s[0]!='-')&&(t[0]!='-'))) return u;
	return '-'+u;
}

string intDiv(string s,string t)
{
int i,j,d;
bool f;
string w,u,v,p,q;
	if (t=="0") return "Divided by 0!";
	if (s=="0") return "0";
	p=Abs(s);
	q=Abs(t);
	if (comp(p,q)==2) return "1";
	else if (comp(p,q)==1) return "0";
	v=p;
	d=p.length()-q.length();
	for (j=0;j<=d;j++)
	{
		w=AddZero(q,d-j);
		for (i=9;i>=0;i--)
			if (comp(v,SinMul(w,i))!=1)
			{
				u=u+char(i+48);
				break;
			}
		v=absMinus(v,SinMul(w,i));
	}
	f=false;
	v="";
	for (i=0;i<u.length();i++)
	{
		if (u[i]!='0') {
			f=true;
			v=v+u[i];
		}
		if ((u[i]=='0')&&(f)) v=v+u[i];
	}
	if (((s[0]=='-')&&(t[0]=='-'))||((s[0]!='-')&&(t[0]!='-'))) return v;
	return '-'+v;
}

string priMod(string s,string t)
{
	if (t=="0") return "Divided by 0!";
	return Minus(s,Multi(intDiv(s,t),t));
}

string Div(string s,string t)
{
	if (t=="0") return "Divided by 0!";
	if ((s[0]!='-')||(priMod(s,t)=="0")) return intDiv(s,t);
	if (t[0]=='-') return posPlus(intDiv(s,t),"1");
	return Plus(intDiv(s,t),"-1");
}

string Mod(string s,string t)
{
	if (t=="0") return "Divided by 0!";
	if ((s[0]!='-')||(priMod(s,t)=="0")) return priMod(s,t);
	if (t[0]=='-') return Minus(priMod(s,t),t);
	return Plus(priMod(s,t),t);
}

string floatDiv(string s,string t,int a)
{
int i;
string w,u,v,q;
	if (t=="0") return "Divided by 0!";
	if (s=="0") return "0";
	q=Abs(t);
	w=priMod(Abs(s),q);
	if (w=="0") return intDiv(s,t);
	u=intDiv(s,t);
	if (a>0) u=u+'.';
	for (i=0;i<a;i++)
	{
		w=w+'0';
		v=intDiv(w,q);
		u=u+v;
		w=absMinus(w,Multi(q,v));
		if (w=="0") break;
	}
	return u;
}

string Divide(string s,string t,int a=4)
{
int i,d;
	if (s=="0") return "0";
	if (comp(s,t)!=1) return floatDiv(s,t,a);
	d=Abs(t).length()-Abs(s).length();
	s=AddZero(s,d);
	if (comp(s,t)==1)
	{
		d=d+1;
		s=s+'0';
	}
	return floatDiv(s,t,a)+"e-"+Str(d);
}

string iGcd(string s,string t)
{
	if (t=="0") return s;
	return iGcd(t,Mod(s,t));
}

string Gcd(string s,string t)
{
	if ((s=="0")||(t=="0")) return "Invalid!";
	if (comp(s,t)==1) return iGcd(Abs(t),Abs(s));
	return iGcd(Abs(s),Abs(t));
}

string Lcd(string s,string t)
{
	if ((s=="0")||(t=="0")) return "0";
	return Abs(Divide(Multi(s,t),Gcd(s,t)));
}

string Square(string s)
{
	return Multi(s,s);
}

string posPower(string s,int a)
{
	if ((s=="0")&&(a==0)) return "Invalid!";
	if (a==0) return "1";
	if (a==1) return s;
	return Multi(posPower(s,a/2),posPower(s,a-a/2));
}

string Power(string s,int a=2,int b=4)
{
	if ((s=="0")&&(a<0)) return "Invalid!";
	if (s=="1") return "1";
	if ((s=="-1")&&(a%2==1)) return "-1";
	if ((s=="-1")&&(a%2==0)) return "1";
	if (a>=0) return posPower(s,a);
	return Divide("1",posPower(s,-a),b);
}

string Sqrt(string s,int a=4)
{
int i,d;
string u,l,r,m,p;
	if (s=="0") return "0";
	p=AddZero(Abs(s),2*a);
	if (p.length()%2==0)
	{
		l=AddZero("3",p.length()/2-1);
		r=AddZero("1",p.length()/2);
	}
	else
	{
		l=AddZero("1",p.length()/2);
		r=AddZero("4",p.length()/2);
	}
	while (comp(l,r)!=2)
	{
		m=intDiv(posPlus(l,r),"2");
		d=comp(Square(m),p);
		if (d==2)
		{
			for (i=0;i<m.length()-a;i++) u=u+m[i];
			return u;
		}
		if (d==1) l=posPlus(m,"1");
		else r=m;
	}
	for (i=0;i<l.length()-a;i++) u=u+l[i];
	if (a>0) u=u+'.';
	for (i=l.length()-a;i<l.length();i++) u=u+l[i];
	if (s[0]=='-') return u+'i';
	return u;
}

string nthRoot(string s,int a=2,int b=4)
{
int i,d,x;
string u,l,r,m,p;
	if (s=="1") return "1";
	if ((s=="-1")&&(a%2==1)) return "-1";
	if (a==1) return s;
	if (a==2) return Sqrt(s,b);
	if ((a==0)||((s[0]=='-')&&(a%2==0))||((s=="0")&&(a<0))) return "Invalid!";
	if (a<0) x=-a; else x=a;
	if (s=="0") return "0";
	p=AddZero(Abs(s),x*b);
	l=AddZero("1",(p.length()-1)/x);
	r=l+'0';
	while (comp(l,r)!=2)
	{
		m=intDiv(posPlus(l,r),"2");
		d=comp(Power(m,x),p);
		if (d==2)
		{
			for (i=0;i<m.length()-b;i++) u=u+m[i];
			if (a<0) return Divide("1",u,b);
			return u;
		}
		if (d==1) l=posPlus(m,"1");
		else r=m;
	}
	if (a>0)
	{
		for (i=0;i<l.length()-b;i++) u=u+l[i];
		if (b>0) u=u+'.';
		for (i=l.length()-b;i<l.length();i++) u=u+l[i];
	}
	else
	{
		r=AddZero("1",b);
		u=Divide(r,l,b);
	}
	if (s[0]=='-') return '-'+u;
	return u;
}

string Log(string s,int a=2)
{
int l,r,m;
bool f;
string w,v,p,q;
	if (s=="1") return "0";
	if ((s=="0")||(s[0]=='-')||(a<=1)) return "Invalid!";
	l=100;
	r=1000;
	v=Str(a);
	if (comp(s,v)==1)
	{
		f=true;
		p=v;
		v=s;
		s=p;
	}
	else f=false;
	if (comp(s,Power(v,10))!=0)
	{
		p=Power(s,100);
		q=Power(v,r);
		while (l!=r)
		{
			m=(l+r)/2;
			w=Power(v,m);
			if (comp(w,p)==2) return f?Divide("100",Str(m)):Divide(Str(m),"100");
			if (comp(w,p)==1) l=m+1;
			else r=m;
		}
		return f?Divide("100",Str(m),2):Divide(Str(l),"100");
	}
	else
	{
		l=10;
		if (comp(s,Power(v,100))!=0)
		{
			p=Power(s,10);
			q=Power(v,r);
			while (l!=r)
			{
				m=(l+r)/2;
				w=Power(v,m);
				if (comp(w,p)==2) return f?Divide("10",Str(m)):Divide(Str(m),"10");
				if (comp(w,p)==1) l=m+1;
				else r=m;
			}
			return f?Divide("10",Str(m),2):Divide(Str(l),"10");
		}
		else
		{
			l=1;
			if (comp(s,Power(v,1000))!=0)
			{
				p=s;
				q=Power(v,r);
				while (l!=r)
				{
					m=(l+r)/2;
					w=Power(v,m);
					if (comp(w,p)==2) return f?Divide("1",Str(m)):Str(m);
					if (comp(w,p)==1) l=m+1;
					else r=m;
				}
				return f?Divide("1",Str(m),2):Str(l);
			}
			else return "Too large!";
		}
	}
}

string iLn(string s,int a)
{
int l,r,m;
string w,v,p,q;
	l=1;
	r=1000;
	v=Str(a);
	if (comp(s,Power(v,10))!=0)
	{
		p=Power(s,100);
		q=Power(v,r);
		while (l!=r)
		{
			m=(l+r)/2;
			w=Power(v,m);
			if (comp(w,p)==2) return Str(m)+"--";
			if (comp(w,p)==1) l=m+1;
			else r=m;
		}
		return Str(l)+"--";
	}
	else
	{
		if (comp(s,Power(v,100))!=0)
		{
			p=Power(s,10);
			q=Power(v,r);
			while (l!=r)
			{
				m=(l+r)/2;
				w=Power(v,m);
				if (comp(w,p)==2) return Str(m)+"-";
				if (comp(w,p)==1) l=m+1;
				else r=m;
			}
			return Str(l)+"-";
		}
		else
		{
			if (comp(s,Power(v,1000))!=0)
			{
				p=s;
				q=Power(v,r);
				while (l!=r)
				{
					m=(l+r)/2;
					w=Power(v,m);
					if (comp(w,p)==2) return Str(m);
					if (comp(w,p)==1) l=m+1;
					else r=m;
				}
				return Str(l);
			}
			else return "Too large!";
		}
	}
}

string Ln(string s)
{
int i,a,b,d;
string w,v,p,q;
	if (s=="1") return "0";
	if ((s=="0")||(s[0]=='-')) return "Invalid!";
	if (Digit(s)<=3) d=1;
	else if (Digit(s)<=7) d=2;
	else if (Digit(s)<=10) d=3;
	else d=4;
	if (d==1)
	{
		w=iLn(s,2);
		v=iLn(s,4);
	}
	else if ((d==2)||(d==3))
	{
		w=iLn(s,5);
		v=iLn(s,10);
	}
	else
	{
		w=iLn(s,10);
		v=iLn(s,30);
	}
	if ((w=="Too large!")||(v=="Too Large!")) return "Too large!";
	if ((w[w.length()-2]=='-')&&(w[w.length()-1]=='-')) a=0;
	else if (w[w.length()-1]=='-') a=1;
	else a=2;
	if ((v[v.length()-2]=='-')&&(v[v.length()-1]=='-')) b=0;
	else if (v[v.length()-1]=='-') b=1;
	else b=2;
	for (i=0;i<w.length();i++)
		if (w[i]!='-') p=p+w[i];
	p=AddZero(p,a);
	for (i=0;i<v.length();i++)
		if (v[i]!='-') q=q+v[i];
	q=AddZero(q,b);
	if (d==1) return Divide(posPlus(Multi(p,"386294"),Multi(q,"613706")),"100000000",2);
	if (d==2) return Divide(posPlus(Multi(p,"70258"),Multi(q,"129742")),"10000000",2);
	if (d==3) return Divide(posPlus(Multi(p,"70258"),Multi(q,"129742")),"10000000",1);
	return Divide(posPlus(Multi(p,"84087"),Multi(q,"215913")),"10000000",1);
}

string Factor(int a)
{
	if (a<0) return "Invalid!";
	if (a==0) return "1";
	return Multi(Factor(a-1),Str(a));
}

string Exp(int a=1,int b=4)
{
	if (a==0) return "1";
	if (a>0) return Divide(Power("271828182845904523536028747135",a),Power("100000000000000000000000000000",a),b);
	return Divide(Power("100000000000000000000000000000",-a),Power("271828182845904523536028747135",-a),b);
}

int main()
{
int c,d;
string a,b;
	cin>>a;
	cin>>b;
	cin>>c>>d;
	cout<<"Length(a) = "<<Digit(a)<<"\n";
	cout<<"Length(b) = "<<Digit(b)<<"\n";
	cout<<"a+b = "<<Plus(a,b)<<"\n";
	cout<<"a-b = "<<Minus(a,b)<<"\n";
	cout<<"a*b = "<<Multi(a,b)<<"\n";
	cout<<"a div b = "<<Div(a,b)<<"\n";
	cout<<"a mod b = "<<Mod(a,b)<<"\n";
	cout<<"a/b = "<<Divide(a,b,d)<<"\n";
	cout<<"Round(a/b) = "<<Round(Divide(a,b,d))<<"\n";
	cout<<"b div a = "<<Div(b,a)<<"\n";
	cout<<"b mod a = "<<Mod(b,a)<<"\n";
	cout<<"b/a = "<<Divide(b,a,d)<<"\n";
	cout<<"Round(b/a) = "<<Round(Divide(b,a,d))<<"\n";
	cout<<"gcd(a,b) = "<<Gcd(a,b)<<"\n";
	cout<<"lcd(a,b) = "<<Lcd(a,b)<<"\n";
	cout<<"a^2 = "<<Square(a)<<"\n";
	cout<<"b^2 = "<<Square(b)<<"\n";
	cout<<"a^"<<c<<" = "<<Power(a,c,d)<<"\n";
	cout<<"b^"<<c<<" = "<<Power(b,c,d)<<"\n";
	cout<<"sqrt(a) = "<<Sqrt(a,d)<<"\n";
	cout<<"sqrt(b) = "<<Sqrt(b,d)<<"\n";
	cout<<"Round(sqrt(a)) = "<<Round(Sqrt(a,d))<<"\n";
	cout<<"Round(sqrt(b)) = "<<Round(Sqrt(b,d))<<"\n";
	cout<<"a^(1/"<<c<<") = "<<nthRoot(a,c,d)<<"\n";
	cout<<"b^(1/"<<c<<") = "<<nthRoot(b,c,d)<<"\n";
	cout<<"log(a,"<<c<<") = "<<Log(a,c)<<"\n";
	cout<<"log(b,"<<c<<") = "<<Log(b,c)<<"\n";
	cout<<"ln(a) = "<<Ln(a)<<"\n";
	cout<<"ln(b) = "<<Ln(b)<<"\n";
	cout<<"c! = "<<Factor(c)<<"\n";
	cout<<"d! = "<<Factor(d)<<"\n";
	cout<<"(|c*d|)! = "<<Factor(abs(c*d))<<"\n";
	cout<<"e^"<<c<<" = "<<Exp(c,d)<<"\n";
	cout<<"e^"<<-c<<" = "<<Exp(-c,d)<<"\n";
	system("Pause");
}
