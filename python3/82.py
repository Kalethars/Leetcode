# 存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除链表中所有存在数字重复情况的节点，只保留原始链表中 没有重复出现 的数字。
# 返回同样按升序排列的结果链表。
# 示例 1：
# 输入：head = [1,2,3,3,4,4,5]
# 输出：[1,2,5]
# 示例 2：
# 输入：head = [1,1,1,2,3]
# 输出：[2,3]
# 提示：
# 链表中节点数目在范围 [0, 300] 内
# -100 <= Node.val <= 100
# 题目数据保证链表已经按升序排列

from python3.listnode import ListNode


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if head is None or head.next is None:
            return head
        if head.next.val != head.val:
            head.next = self.deleteDuplicates(head.next)
            return head
        value = head.val
        while head is not None and head.val == value:
            head = head.next
        return self.deleteDuplicates(head)


if __name__ == '__main__':
    assert Solution().deleteDuplicates(ListNode.build_chain(1, 2, 3, 3, 4, 4, 5)).get_chain() == [1, 2, 5]
    assert Solution().deleteDuplicates(ListNode.build_chain(1, 1, 1, 2, 3)).get_chain() == [2, 3]
    assert Solution().deleteDuplicates(ListNode.build_chain(1, 1)) is None
