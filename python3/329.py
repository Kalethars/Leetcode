# 给定一个 m x n 整数矩阵 matrix ，找出其中 最长递增路径 的长度。
# 对于每个单元格，你可以往上，下，左，右四个方向移动。 你 不能 在 对角线 方向上移动或移动到 边界外（即不允许环绕）。
# 示例 1：
# 输入：matrix = [[9,9,4],[6,6,8],[2,1,1]]
# 输出：4 
# 解释：最长递增路径为 [1, 2, 6, 9]。
# 示例 2：
# 输入：matrix = [[3,4,5],[3,2,6],[2,2,1]]
# 输出：4 
# 解释：最长递增路径是 [3, 4, 5, 6]。注意不允许在对角线方向上移动。
# 示例 3：
# 输入：matrix = [[1]]
# 输出：1
# 提示：
# m == matrix.length
# n == matrix[i].length
# 1 <= m, n <= 200
# 0 <= matrix[i][j] <= 2^31 - 1

from typing import List


class Solution:
    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:
        self.matrix = matrix
        self.paths = [[0] * len(row) for row in matrix]
        max_path = 1
        for row_index, row in enumerate(matrix):
            for column_index in range(len(row)):
                max_path = max(self.find_longest_path(row_index, column_index), max_path)
        return max_path

    def find_longest_path(self, row_index, column_index):
        if self.paths[row_index][column_index] == 0:
            max_path = 1
            for delta_row, delta_column in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
                next_row_index, next_column_index = row_index + delta_row, column_index + delta_column
                if 0 <= next_row_index < len(self.matrix) and 0 <= next_column_index < len(self.matrix[0]) \
                        and self.matrix[next_row_index][next_column_index] > self.matrix[row_index][column_index]:
                    max_path = max(self.find_longest_path(next_row_index, next_column_index) + 1, max_path)
            self.paths[row_index][column_index] = max_path
        return self.paths[row_index][column_index]


if __name__ == '__main__':
    assert Solution().longestIncreasingPath([[9, 9, 4], [6, 6, 8], [2, 1, 1]]) == 4
    assert Solution().longestIncreasingPath([[3, 4, 5], [3, 2, 6], [2, 2, 1]]) == 4
    assert Solution().longestIncreasingPath([[1]]) == 1
