# 给定两个整数，被除数 dividend 和除数 divisor。将两数相除，要求不使用乘法、除法和 mod 运算符。
# 返回被除数 dividend 除以除数 divisor 得到的商。
# 整数除法的结果应当截去（truncate）其小数部分，例如：truncate(8.345) = 8 以及 truncate(-2.7335) = -2


class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        negative = (dividend > 0) ^ (divisor > 0)
        dividend, divisor = abs(dividend), abs(divisor)
        values, multiplier = [], 1
        while divisor <= dividend:
            values.append((divisor, multiplier))
            divisor, multiplier = divisor << 1, multiplier << 1
        quotient = 0
        for value, multiplier in values[::-1]:
            if dividend >= value:
                quotient += multiplier
                dividend -= value
        return -quotient if negative else min(quotient, (1 << 31) - 1)


if __name__ == '__main__':
    assert Solution().divide(10, 3) == 3
    assert Solution().divide(7, -3) == -2
