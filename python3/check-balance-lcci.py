# 实现一个函数，检查二叉树是否平衡。在这个问题中，平衡树的定义如下：任意一个节点，其两棵子树的高度差不超过 1。
# 示例 1:
# 给定二叉树 [3,9,20,null,null,15,7]
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 返回 true 。
# 示例 2:
# 给定二叉树 [1,2,2,3,3,null,null,4,4]
#       1
#      / \
#     2   2
#    / \
#   3   3
#  / \
# 4   4
# 返回 false 。

from python3.treenode import TreeNode


class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        return self.balance_and_depth(root)[0]

    def balance_and_depth(self, node: TreeNode):
        if node is None:
            return True, 0
        left_balance, left_depth = self.balance_and_depth(node.left)
        right_balance, right_depth = self.balance_and_depth(node.right)
        return left_balance and right_balance and abs(left_depth - right_depth) <= 1, max(left_depth, right_depth) + 1


if __name__ == '__main__':
    assert Solution().isBalanced(TreeNode.build_tree([3, 9, 20, None, None, 15, 7])) is True
    assert Solution().isBalanced(TreeNode.build_tree([1, 2, 2, 3, 3, None, None, 4, 4])) is False
