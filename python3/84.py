# 给定 n 个非负整数，用来表示柱状图中各个柱子的高度。每个柱子彼此相邻，且宽度为 1 。
# 求在该柱状图中，能够勾勒出来的矩形的最大面积。
# 示例 1:
# 输入：heights = [2,1,5,6,2,3]
# 输出：10
# 解释：最大的矩形为图中红色区域，面积为 10
# 示例 2：
# 输入： heights = [2,4]
# 输出： 4
# 提示：
# 1 <= heights.length <=10^5
# 0 <= heights[i] <= 10^4

from typing import List


class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:
        left_borders = [-1]  # left_borders保持单调不下降高度的索引
        max_area = 0
        # 附加 0 作为终止符
        for index, height in enumerate(heights + [0]):
            # 拿出所有比当前高度更高的进行面积计算
            while len(left_borders) > 1 and height < heights[left_borders[-1]]:
                # width = (index - 1) - (left_borders[-1] + 1) + 1 = index - left_borders[-1] - 1
                max_area = max(heights[left_borders.pop()] * (index - left_borders[-1] - 1), max_area)
            left_borders.append(index)
        return max_area


if __name__ == '__main__':
    assert Solution().largestRectangleArea([2, 1, 5, 6, 2, 3]) == 10
    assert Solution().largestRectangleArea([2, 4]) == 4
    assert Solution().largestRectangleArea([1, 2, 2]) == 4
    assert Solution().largestRectangleArea([1, 2, 3, 4, 5]) == 9
    assert Solution().largestRectangleArea([3, 4, 6, 7, 5, 4]) == 20
