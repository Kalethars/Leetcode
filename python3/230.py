# 给定一个二叉搜索树的根节点 root ，和一个整数 k ，请你设计一个算法查找其中第 k 个最小元素（从 1 开始计数）。
# 示例 1：
# 输入：root = [3,1,4,null,2], k = 1
# 输出：1
# 示例 2：
# 输入：root = [5,3,6,2,4,null,null,1], k = 3
# 输出：3
# 提示：
# 树中的节点数为 n 。
# 1 <= k <= n <= 10^4
# 0 <= Node.val <= 10^4
# 进阶：如果二叉搜索树经常被修改（插入/删除操作）并且你需要频繁地查找第 k 小的值，你将如何优化算法？

from python3.treenode import TreeNode


class Solution:
    def kthSmallest(self, root: TreeNode, k: int) -> int:
        return self.find_kth(root, k)[0]

    def find_kth(self, tree: TreeNode, k: int):
        if tree is None:
            return None, 0
        left_value, left_count = self.find_kth(tree.left, k)
        if left_value is not None:
            return left_value, None
        if left_count == k - 1:
            return tree.val, None
        right_value, right_count = self.find_kth(tree.right, k - left_count - 1)
        if right_value is not None:
            return right_value, None
        return None, left_count + right_count + 1


if __name__ == '__main__':
    assert Solution().kthSmallest(TreeNode.build_tree([3, 1, 4, None, 2]), 1) == 1
    assert Solution().kthSmallest(TreeNode.build_tree([5, 3, 6, 2, 4, None, None, 1]), 3) == 3
