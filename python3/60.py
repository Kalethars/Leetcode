# 给出集合[1,2,3,...,n]，其所有元素共有 n! 种排列。
# 按大小顺序列出所有排列情况，并一一标记，当 n = 3 时, 所有排列如下：
# "123"
# "132"
# "213"
# "231"
# "312"
# "321"
# 给定 n 和 k，返回第k个排列。
# 示例 1：
# 输入：n = 3, k = 3
# 输出："213"
# 示例 2：
# 输入：n = 4, k = 9
# 输出："2314"
# 示例 3：
# 输入：n = 3, k = 1
# 输出："123"
# 提示：
# 1 <= n <= 9
# 1 <= k <= n!

import math

class Solution:
    def getPermutation(self, n: int, k: int) -> str:
        numbers = [i + 1 for i in range(n)]
        result = ''
        for i in range(n - 1, -1, -1):
            result += str(numbers.pop((k - 1) // math.factorial(i)))
            k %= math.factorial(i)
        return result


if __name__ == '__main__':
    assert Solution().getPermutation(3, 3) == '213'
    assert Solution().getPermutation(4, 9) == '2314'
    assert Solution().getPermutation(3, 1) == '123'
