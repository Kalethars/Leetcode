# 给定一个包含红色、白色和蓝色，一共 n 个元素的数组，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。
# 此题中，我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。
# 示例 1：
# 输入：nums = [2,0,2,1,1,0]
# 输出：[0,0,1,1,2,2]
# 示例 2：
# 输入：nums = [2,0,1]
# 输出：[0,1,2]
# 示例 3：
# 输入：nums = [0]
# 输出：[0]
# 示例 4：
# 输入：nums = [1]
# 输出：[1]
# 提示：
# n == nums.length
# 1 <= n <= 300
# nums[i] 为 0、1 或 2

from typing import List


class Solution:
    def sortColors(self, nums: List[int]) -> None:
        left, right = 0, len(nums) - 1
        for index in range(len(nums)):
            if index > right:
                break
            while True:
                swap_left, swap_right = index > left and nums[index] == 0, index < right and nums[index] == 2
                if swap_left:
                    nums[left], nums[index] = nums[index], nums[left]
                    left += 1
                if swap_right:
                    nums[right], nums[index] = nums[index], nums[right]
                    right -= 1
                if not (swap_left or swap_right):
                    break


if __name__ == '__main__':
    nums = [2, 0, 2, 1, 1, 0]
    Solution().sortColors(nums)
    assert nums == [0, 0, 1, 1, 2, 2]

    nums = [2, 0, 1]
    Solution().sortColors(nums)
    assert nums == [0, 1, 2]

    nums = [0]
    Solution().sortColors(nums)
    assert nums == [0]

    nums = [1]
    Solution().sortColors(nums)
    assert nums == [1]
