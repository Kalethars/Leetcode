# 给定一个字符串 s 和一些长度相同的单词 words。找出 s 中恰好可以由 words 中所有单词串联形成的子串的起始位置。
# 注意子串要与 words 中的单词完全匹配，中间不能有其他字符，但不需要考虑 words 中单词串联的顺序。

from typing import List


class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        if len(words) == 0:
            return []
        length = len(words[0])
        if len(s) < len(words) * length:
            return []
        counts = dict()
        for word in words:
            counts[word] = counts.get(word, 0) + 1
        indices, matched_words_by_index = dict(), dict()
        for head_index in range(len(s) - length + 1):
            partial_string = s[head_index:head_index + length]
            if partial_string in counts:
                matched_words_by_index[head_index] = partial_string
                if partial_string not in indices:
                    indices[partial_string] = []
                indices[partial_string].append(head_index)
        for word, index_list in indices.items():
            if len(index_list) == 0:
                return []
            for index in index_list:
                matched_words_by_index[index] = word
        anchor_word = min(indices.keys(), key=lambda word: len(indices[word]) - counts[word])
        results, anchor_indices_occurred = set(), set()
        for anchor_index in indices[anchor_word]:
            if anchor_index in anchor_indices_occurred:
                continue
            anchor_indices_occurred.add(anchor_index)
            remained_counts = {word: count for word, count in counts.items()}
            head_index = max(anchor_index % length, anchor_index - length * (len(words) - 1))
            tail_index = head_index + length * len(words)
            for index in range(head_index, tail_index, length):
                if index in matched_words_by_index:
                    remained_counts[matched_words_by_index[index]] -= 1
                    if matched_words_by_index[index] == anchor_word:
                        anchor_indices_occurred.add(index)
            if all(count == 0 for count in remained_counts.values()):
                results.add(head_index)
            while tail_index < min(anchor_index + length * len(words), len(s)):
                removed_index = tail_index - length * len(words)
                if tail_index in matched_words_by_index:
                    remained_counts[matched_words_by_index[tail_index]] -= 1
                    if matched_words_by_index[tail_index] == anchor_word:
                        anchor_indices_occurred.add(tail_index)
                if removed_index in matched_words_by_index:
                    remained_counts[matched_words_by_index[removed_index]] += 1
                tail_index += length
                if all(count == 0 for count in remained_counts.values()):
                    results.add(removed_index + length)
        return list(results)


if __name__ == '__main__':
    assert sorted(Solution().findSubstring('barfoothefoobarman', ['foo', 'bar'])) == [0, 9]
    assert sorted(Solution().findSubstring('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'word'])) == []
