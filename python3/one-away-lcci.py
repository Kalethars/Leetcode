# 字符串有三种编辑操作:插入一个字符、删除一个字符或者替换一个字符。 给定两个字符串，编写一个函数判定它们是否只需要一次(或者零次)编辑。
# 示例 1:
# 输入: 
# first = "pale"
# second = "ple"
# 输出: True
# 示例 2:
# 输入: 
# first = "pales"
# second = "pal"
# 输出: False

class Solution:
    def oneEditAway(self, first: str, second: str) -> bool:
        if abs(len(first) - len(second)) > 1:
            return False
        if len(first) < len(second):
            first, second = second, first
        for index in range(len(second)):
            if first[index] != second[index]:
                return first[index + 1:] == second[index if len(first) > len(second) else index + 1:]
        return True


if __name__ == '__main__':
    assert Solution().oneEditAway('pale', 'ple') is True
    assert Solution().oneEditAway('pales', 'pal') is False
