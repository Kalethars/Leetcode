# 给定一个可包含重复数字的序列 nums ，按任意顺序 返回所有不重复的全排列。
# 示例 1：
# 输入：nums = [1,1,2]
# 输出：
# [[1,1,2],
#  [1,2,1],
#  [2,1,1]]
# 示例 2：
# 输入：nums = [1,2,3]
# 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
# 提示：
# 1 <= nums.length <= 8
# -10 <= nums[i] <= 10

from typing import List


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        return sum([[[nums[i]] + permutation for permutation in self.permuteUnique(nums[:i] + nums[i + 1:])]
                    for i in range(len(nums)) if i == 0 or nums[i] > nums[i - 1]], []) if len(nums) > 1 else [nums]


if __name__ == '__main__':
    assert len(Solution().permuteUnique([1, 1, 2])) == 3
    assert len(Solution().permuteUnique([1, 2, 3])) == 6
