# 从左向右遍历一个数组，通过不断将其中的元素插入树中可以逐步地生成一棵二叉搜索树。给定一个由不同节点组成的二叉搜索树，输出所有可能生成此树的数组。
# 示例：
# 给定如下二叉树
# 
#         2
#        / \
#       1   3
# 返回：
# [
#    [2,1,3],
#    [2,3,1]
# ]
from typing import List

from python3.treenode import TreeNode


class Solution:
    def BSTSequences(self, root: TreeNode) -> List[List[int]]:
        if root is None:
            return [[]]
        elif (root.left or root.right) is None:
            return [[root.val]]
        self.sequences = []
        self.dfs([root], [root.val])
        return self.sequences

    def dfs(self, parents: List[TreeNode], sequence: List[int]):
        if len(parents) == 0:
            return self.sequences.append(sequence)
        for index, parent in enumerate(parents):
            for child1, child2 in [(parent.left, parent.right), (parent.right, parent.left)]:
                if child1 is not None:
                    next_parents = parents[:index] + parents[index + 1:]
                    if (child1.left or child1.right) is not None:
                        next_parents.append(child1)
                    if child2 is not None:
                        dummy_parent = TreeNode(parent.val)
                        dummy_parent.right = child2
                        next_parents.append(dummy_parent)
                    self.dfs(next_parents, sequence + [child1.val])


if __name__ == '__main__':
    assert len(Solution().BSTSequences(TreeNode.build_tree([2, 1, 3]))) == 2
    assert len(Solution().BSTSequences(TreeNode.build_tree([5, 2, None, 1, 4, None, None, None, None, 3]))) == 3
