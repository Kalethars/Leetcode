# 实现获取下一个排列的函数，算法需要将给定数字序列重新排列成字典序中下一个更大的排列。
# 如果不存在下一个更大的排列，则将数字重新排列成最小的排列（即升序排列）。
# 必须原地修改，只允许使用额外常数空间。

from typing import List


class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        index_for_swap = -1
        for index in range(len(nums) - 2, -1, -1):
            if nums[index + 1] > nums[index]:
                index_for_swap = index
                break
        if index_for_swap == -1:
            nums.reverse()
            return
        for index in range(len(nums) - 1, index_for_swap, -1):
            if nums[index] > nums[index_for_swap]:
                nums[index], nums[index_for_swap] = nums[index_for_swap], nums[index]
                nums[index_for_swap + 1:] = nums[index_for_swap + 1:][::-1]
                return


if __name__ == '__main__':
    nums = [1, 2, 3]
    Solution().nextPermutation(nums)
    assert nums == [1, 3, 2]
    nums = [3, 2, 1]
    Solution().nextPermutation(nums)
    assert nums == [1, 2, 3]
    nums = [1, 1, 5]
    Solution().nextPermutation(nums)
    assert nums == [1, 5, 1]
    nums = [1, 3, 2]
