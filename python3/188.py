# 给定一个整数数组 prices ，它的第 i 个元素 prices[i] 是一支给定的股票在第 i 天的价格。
# 设计一个算法来计算你所能获取的最大利润。你最多可以完成 k 笔交易。
# 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
# 示例 1：
# 输入：k = 2, prices = [2,4,1]
# 输出：2
# 解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2 。
# 示例 2：
# 输入：k = 2, prices = [3,2,6,5,0,3]
# 输出：7
# 解释：在第 2 天 (股票价格 = 2) 的时候买入，在第 3 天 (股票价格 = 6) 的时候卖出, 这笔交易所能获得利润 = 6-2 = 4 。
#      随后，在第 5 天 (股票价格 = 0) 的时候买入，在第 6 天 (股票价格 = 3) 的时候卖出, 这笔交易所能获得利润 = 3-0 = 3 。
# 提示：
# 0 <= k <= 100
# 0 <= prices.length <= 1000
# 0 <= prices[i] <= 1000

import heapq
from typing import List


class Solution:
    def maxProfit(self, k: int, prices: List[int]) -> int:
        valley_peak_pairs, profits = [], []  # 保留在峰谷对中的前后对子必定满足前者包含后者
        valley_index, peak_index = -1, -1
        while valley_index < len(prices) and peak_index < len(prices):
            valley_index = peak_index + 1
            while valley_index < len(prices) - 1 and prices[valley_index] >= prices[valley_index + 1]:
                valley_index += 1
            peak_index = valley_index
            while peak_index < len(prices) - 1 and prices[peak_index] <= prices[peak_index + 1]:
                peak_index += 1
            if valley_index >= len(prices) - 1 and peak_index >= len(prices) - 1:
                break
            valley_price, peak_price = prices[valley_index], prices[peak_index]
            while len(valley_peak_pairs) > 0 and valley_peak_pairs[-1][0] > valley_price:  # 之前的谷更高，无法套利，弹出
                profits.append(valley_peak_pairs[-1][1] - valley_peak_pairs[-1][0])
                valley_peak_pairs.pop()
            while len(valley_peak_pairs) > 0 and valley_peak_pairs[-1][1] < peak_price:  # 之前的峰更低，交换交易顺序
                profits.append(valley_peak_pairs[-1][1] - valley_price)
                valley_price = valley_peak_pairs.pop()[0]
            valley_peak_pairs.append((valley_price, peak_price))  # 最后一个的峰一定高于之后查询到的谷
        profits += [peak_price - valley_price for valley_price, peak_price in valley_peak_pairs]
        return sum(heapq.nlargest(min(k, len(profits)), profits))

    def maxProfit_kn(self, k: int, prices: List[int]) -> int:
        if k >= len(prices) // 2:
            return sum([max(prices[i + 1] - prices[i], 0) for i in range(len(prices) - 1)])
        max_profits = [0] * len(prices)
        for _ in range(k):
            max_profit = 0
            for i in range(1, len(prices)):
                max_profit = max(max_profit + prices[i] - prices[i - 1], max_profits[i])
                max_profits[i] = max(max_profits[i - 1], max_profit)
        return max_profits[-1]


if __name__ == '__main__':
    assert Solution().maxProfit(2, [2, 4, 1]) == 2
    assert Solution().maxProfit(2, [3, 2, 6, 5, 0, 3]) == 7
    assert Solution().maxProfit(2, [0, 8, 5, 7, 4, 7]) == 11
