# 假设按照升序排序的数组在预先未知的某个点上进行了旋转。
# ( 例如，数组 [0,1,2,4,5,6,7] 可能变为 [4,5,6,7,0,1,2] )。
# 搜索一个给定的目标值，如果数组中存在这个目标值，则返回它的索引，否则返回 -1 。
# 你可以假设数组中不存在重复的元素。
# 你的算法时间复杂度必须是 O(log n) 级别。

from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        if len(nums) == 0:
            return -1
        if nums[0] == target:
            return 0
        elif nums[-1] == target:
            return len(nums) - 1
        mid = len(nums) // 2
        if nums[mid] == target:
            return mid
        elif nums[-1] < target < nums[0]:
            return -1
        elif (nums[0] < target < nums[mid]) or (target < nums[mid] < nums[-1]) or (nums[mid] < nums[0] < target):
            return self.search(nums[:mid], target)
        else:
            index = self.search(nums[mid + 1:], target)
            return mid + 1 + index if index >= 0 else -1


if __name__ == '__main__':
    assert Solution().search([4, 5, 6, 7, 0, 1, 2], 0) == 4
    assert Solution().search([4, 5, 6, 7, 0, 1, 2], 3) == -1
