# 给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。
#
# 示例：
# 给定一个链表: 1->2->3->4->5, 和 n = 2.
# 当删除了倒数第二个节点后，链表变为 1->2->3->5.
#
# 说明：# 给定的 n 保证是有效的。
#
# 进阶：你能尝试使用一趟扫描实现吗？

from python3.listnode import ListNode


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        nodes = [head]
        while head is not None:
            nodes.append(head.next)
            head = head.next
        if n == len(nodes) - 1:
            return nodes[1]
        nodes[-n - 2].next = nodes[-n - 1].next
        return nodes[0]


if __name__ == '__main__':
    assert Solution().removeNthFromEnd(ListNode.build_chain(1, 2, 3, 4, 5), 2).get_chain() == [1, 2, 3, 5]
