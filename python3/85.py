# 给定一个仅包含 0 和 1 、大小为 rows x cols 的二维二进制矩阵，找出只包含 1 的最大矩形，并返回其面积。
# 示例 1：
# 输入：matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
# 输出：6
# 解释：最大矩形如上图所示。
# 示例 2：
# 输入：matrix = []
# 输出：0
# 示例 3：
# 输入：matrix = [["0"]]
# 输出：0
# 示例 4：
# 输入：matrix = [["1"]]
# 输出：1
# 示例 5：
# 输入：matrix = [["0","0"]]
# 输出：0
# 提示：
# 
# rows == matrix.length
# cols == matrix[0].length
# 0 <= row, cols <= 200
# matrix[i][j] 为 '0' 或 '1'

from typing import List


class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:
        if len(matrix) == 0:
            return 0
        heights = [0] * len(matrix[0])
        max_area = 0
        for row in matrix:
            heights = [0 if int(value) == 0 else height + 1 for value, height in zip(row, heights)]
            max_area = max(self.largestRectangleArea(heights), max_area)
        return max_area

    # 引用84题
    def largestRectangleArea(self, heights: List[int]) -> int:
        left_borders = [-1]  # left_borders保持单调不下降高度的索引
        max_area = 0
        # 附加 0 作为终止符
        for index, height in enumerate(heights + [0]):
            # 拿出所有比当前高度更高的进行面积计算
            while len(left_borders) > 1 and height < heights[left_borders[-1]]:
                # width = (index - 1) - (left_borders[-1] + 1) + 1 = index - left_borders[-1] - 1
                max_area = max(heights[left_borders.pop()] * (index - left_borders[-1] - 1), max_area)
            left_borders.append(index)
        return max_area


if __name__ == '__main__':
    assert Solution().maximalRectangle([['1', '0', '1', '0', '0'],
                                        ['1', '0', '1', '1', '1'],
                                        ['1', '1', '1', '1', '1'],
                                        ['1', '0', '0', '1', '0']]) == 6
    assert Solution().maximalRectangle([]) == 0
    assert Solution().maximalRectangle([['0']]) == 0
    assert Solution().maximalRectangle([['1']]) == 1
    assert Solution().maximalRectangle([['0', '0']]) == 0
