# 给你一个正整数 n ，生成一个包含 1 到 n^2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。
# 示例 1：
# 输入：n = 3
# 输出：[[1,2,3],[8,9,4],[7,6,5]]
# 示例 2：
# 输入：n = 1
# 输出：[[1]]
# 提示：
# 1 <= n <= 20

from typing import List


class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:
        return [] if n == 0 else [[1]] if n == 1 else [
            [i + 1 for i in range(n)],
            *[[4 * n - 4 - i, *[value + 4 * n - 4 for value in line], n + i + 1]
              for i, line in enumerate(self.generateMatrix(n - 2))],
            [3 * n - i - 2 for i in range(n)]
        ]


if __name__ == '__main__':
    assert Solution().generateMatrix(3) == [[1, 2, 3], [8, 9, 4], [7, 6, 5]]
    assert Solution().generateMatrix(1) == [[1]]
