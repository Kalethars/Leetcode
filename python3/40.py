# 给定一个数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
# candidates 中的每个数字在每个组合中只能使用一次。
#
# 说明：
# 所有数字（包括目标数）都是正整数。
# 解集不能包含重复的组合。

from typing import List


class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        self.candidates = sorted(candidates)
        self.results = dict()
        return self.get_combinations(target, len(self.candidates))

    def get_combinations(self, target, right_index):
        from bisect import bisect_right
        if (target, right_index) in self.results:
            return self.results[(target, right_index)]
        results = []
        start_index = min(bisect_right(self.candidates, target), right_index) - 1
        for index in range(start_index, -1, -1):
            candidate = self.candidates[index]
            if index < start_index and candidate == self.candidates[index + 1]:
                continue
            if candidate == target:
                results.append([candidate])
            else:
                results.extend([[candidate, *result] for result in self.get_combinations(target - candidate, index)])
        self.results[(target, right_index)] = results
        return results

    def save_results(self, target, index, results):
        if target not in self.results:
            self.results[target] = dict()
        self.results[target][index] = results


if __name__ == '__main__':
    assert len(Solution().combinationSum2([10, 1, 2, 7, 6, 1, 5], 8)) == 4
    assert len(Solution().combinationSum2([2, 5, 2, 1, 2], 5)) == 2
