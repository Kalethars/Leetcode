# 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
# 示例 1：
# 输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
# 输出：6
# 解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
# 示例 2：
# 输入：nums = [1]
# 输出：1
# 示例 3：
# 输入：nums = [0]
# 输出：0
# 示例 4：
# 输入：nums = [-1]
# 输出：-1
# 示例 5：
# 输入：nums = [-100000]
# 输出：-100000
# 提示：
# 1 <= nums.length <= 3 * 10^4
# -10^5 <= nums[i] <= 10^5

from typing import List


class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        local_max_sum, global_max_sum = nums[0], nums[0]
        for num in nums[1:]:
            local_max_sum = max(num, local_max_sum + num)
            global_max_sum = max(global_max_sum, local_max_sum)
        return global_max_sum


if __name__ == '__main__':
    assert Solution().maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]) == 6
    assert Solution().maxSubArray([1]) == 1
