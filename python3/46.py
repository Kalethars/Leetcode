# 给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
# 示例 1：
# 输入：nums = [1,2,3]
# 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
# 示例 2：
# 输入：nums = [0,1]
# 输出：[[0,1],[1,0]]
# 示例 3：
# 输入：nums = [1]
# 输出：[[1]]
# 提示：
# 1 <= nums.length <= 6
# -10 <= nums[i] <= 10
# nums 中的所有整数 互不相同

from typing import List


class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        return sum([[[nums[i]] + permutation for permutation in self.permute(nums[:i] + nums[i + 1:])]
                    for i in range(len(nums))], []) if len(nums) > 1 else [nums]


if __name__ == '__main__':
    assert len(Solution().permute([1, 2, 3, 4, 5, 6])) == 720
    assert len(Solution().permute([1, 2, 3])) == 6
    assert len(Solution().permute([0, 1])) == 2
    assert len(Solution().permute([1])) == 1
