# 给你一个链表，每k个节点一组进行翻转，请你返回翻转后的链表。
# k是一个正整数，它的值小于或等于链表的长度。
# 如果节点总数不是k的整数倍，那么请将最后剩余的节点保持原有顺序。
# 进阶：
# 你可以设计一个只使用常数额外空间的算法来解决此问题吗？
# 你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。
# 示例 1：
# 输入：head = [1,2,3,4,5], k = 2
# 输出：[2,1,4,3,5]
# 输入：head = [1,2,3,4,5], k = 3
# 输出：[3,2,1,4,5]
# 示例 3：
# 输入：head = [1,2,3,4,5], k = 1
# 输出：[1,2,3,4,5]
# 示例 4：
# 输入：head = [1], k = 1
# 输出：[1]
# 提示：
# 列表中节点的数量在范围 sz 内
# 1 <= sz <= 5000
# 0 <= Node.val <= 1000
# 1 <= k <= sz

from python3.listnode import ListNode


class Solution:
    def reverseKGroup(self, head: ListNode, k: int) -> ListNode:
        if head is None or head.next is None or k == 1:
            return head
        node = head
        for i in range(k - 1):
            node = node.next
            if node is None:
                return head
        reversed_back = self.reverseKGroup(node.next, k)
        node.next = None
        current_head = self.reverse(head)
        head.next = reversed_back
        return current_head

    def reverse(self, head: ListNode):
        if head is None or head.next is None:
            return head
        node, node_to_be_next = head, None
        while node.next is not None:
            node.next, node_to_be_next, node = node_to_be_next, node, node.next
        node.next = node_to_be_next
        return node


if __name__ == '__main__':
    assert Solution().reverseKGroup(ListNode.build_chain(1, 2, 3, 4, 5), 2).get_chain() == [2, 1, 4, 3, 5]
    assert Solution().reverseKGroup(ListNode.build_chain(1, 2, 3, 4, 5), 3).get_chain() == [3, 2, 1, 4, 5]
    assert Solution().reverseKGroup(ListNode.build_chain(1, 2, 3, 4, 5), 1).get_chain() == [1, 2, 3, 4, 5]
    assert Solution().reverseKGroup(ListNode.build_chain(1, 2, 3, 4, 5, 6, 7, 8, 9), 4).get_chain() == \
           [4, 3, 2, 1, 8, 7, 6, 5, 9]
