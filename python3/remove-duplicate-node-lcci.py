# 编写代码，移除未排序链表中的重复节点。保留最开始出现的节点。
# 示例1:
#  输入：[1, 2, 3, 3, 2, 1]
#  输出：[1, 2, 3]
# 示例2:
#  输入：[1, 1, 1, 1, 2]
#  输出：[1, 2]
# 提示：
# 链表长度在[0, 20000]范围内。
# 链表元素在[0, 20000]范围内。
# 进阶：
# 如果不得使用临时缓冲区，该怎么解决？

from python3.listnode import ListNode


class Solution:
    def removeDuplicateNodes(self, head: ListNode) -> ListNode:
        if head is None:
            return None
        values = {head.val}
        node, last_node = head.next, head
        while node is not None:
            if node.val not in values:
                values.add(node.val)
                last_node.next, last_node = node, node
            node = node.next
        last_node.next = None
        return head


if __name__ == '__main__':
    assert Solution().removeDuplicateNodes(ListNode.build_chain(1, 2, 3, 3, 2, 1)).get_chain() == [1, 2, 3]
    assert Solution().removeDuplicateNodes(ListNode.build_chain(1, 1, 1, 1, 2)).get_chain() == [1, 2]
