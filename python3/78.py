# 给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
# 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
# 示例 1：
# 输入：nums = [1,2,3]
# 输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
# 示例 2：
# 输入：nums = [0]
# 输出：[[],[0]]
# 提示：
# 1 <= nums.length <= 10
# -10 <= nums[i] <= 10
# nums 中的所有元素 互不相同

from typing import List


class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        return [[]] + sum([[[num] + subset for subset in self.subsets(nums[i + 1:])] for i, num in enumerate(nums)], [])


if __name__ == '__main__':
    print(Solution().subsets([1, 2, 3]))
    assert len(Solution().subsets([1, 2, 3])) == 8
    assert len(Solution().subsets([0])) == 2
