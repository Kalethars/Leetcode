# 给定三个字符串 s1、s2、s3，请你帮忙验证 s3 是否是由 s1 和 s2 交错 组成的。
# 两个字符串 s 和 t 交错 的定义与过程如下，其中每个字符串都会被分割成若干 非空 子字符串：
# s = s1 + s2 + ... + sn
# t = t1 + t2 + ... + tm
# |n - m| <= 1
# 交错 是 s1 + t1 + s2 + t2 + s3 + t3 + ... 或者 t1 + s1 + t2 + s2 + t3 + s3 + ...
# 提示：a + b 意味着字符串 a 和 b 连接。
# 示例 1：
# 输入：s1 = 'aabcc', s2 = 'dbbca', s3 = 'aadbbcbcac'
# 输出：true
# 示例 2：
# 输入：s1 = 'aabcc', s2 = 'dbbca', s3 = 'aadbbbaccc'
# 输出：false
# 示例 3：
# 输入：s1 = '', s2 = '', s3 = ''
# 输出：true
# 提示：
# 0 <= s1.length, s2.length <= 100
# 0 <= s3.length <= 200
# s1、s2、和 s3 都由小写英文字母组成

from functools import lru_cache


class Solution:
    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        if len(s1) + len(s2) != len(s3):
            return False
        self.s1, self.s2, self.s3 = s1, s2, s3
        return self.is_interleave(len(s1) - 1, len(s2) - 1)

    @lru_cache(None)
    def is_interleave(self, i1, i2):
        return i1 + i2 == -2 \
               or (i1 >= 0 and self.s1[i1] == self.s3[i1 + i2 + 1] and self.is_interleave(i1 - 1, i2)) \
               or (i2 >= 0 and self.s2[i2] == self.s3[i1 + i2 + 1] and self.is_interleave(i1, i2 - 1))


if __name__ == '__main__':
    assert Solution().isInterleave('aabcc', 'dbbca', 'aadbbcbcac') is True
    assert Solution().isInterleave('aabcc', 'dbbca', 'aadbbbaccc') is False
    assert Solution().isInterleave('', '', '') is True
