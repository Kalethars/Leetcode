# 给定一个链表，两两交换其中相邻的节点，并返回交换后的链表。
# 你不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。

from python3.listnode import ListNode


class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        if head is None or head.next is None:
            return head
        next_node = head.next
        head.next = self.swapPairs(next_node.next)
        next_node.next = head
        return next_node


if __name__ == '__main__':
    assert Solution().swapPairs(ListNode.build_chain(1, 2, 3, 4)).get_chain() == [2, 1, 4, 3]
