# 给定两个字符串 s1 和 s2，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串。
# 示例 1：
# 输入: s1 = "abc", s2 = "bca"
# 输出: true
# 示例 2：
# 输入: s1 = "abc", s2 = "bad"
# 输出: false
# 说明：
# 0 <= len(s1) <= 100
# 0 <= len(s2) <= 100

class Solution:
    def CheckPermutation(self, s1: str, s2: str) -> bool:
        if len(s1) != len(s2):
            return False
        counter = dict()
        for c1, c2 in zip(s1, s2):
            counter[c1] = counter.get(c1, 0) + 1
            counter[c2] = counter.get(c2, 0) - 1
        return all(count == 0 for count in counter.values())


if __name__ == '__main__':
    assert Solution().CheckPermutation('abc', 'bca') is True
    assert Solution().CheckPermutation('abc', 'bad') is False
    assert Solution().CheckPermutation('aab', 'abb') is False
