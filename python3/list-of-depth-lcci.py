# 给定一棵二叉树，设计一个算法，创建含有某一深度上所有节点的链表（比如，若一棵树的深度为 D，则会创建出 D 个链表）。返回一个包含所有深度的链表的数组。
# 示例：
# 输入：[1,2,3,4,5,null,7,8]
#
#         1
#        /  \
#       2    3
#      / \    \
#     4   5    7
#    /
#   8
# 输出：[[1],[2,3],[4,5,7],[8]]

from typing import List

from python3.listnode import ListNode
from python3.treenode import TreeNode


class Solution:
    def listOfDepth(self, tree: TreeNode) -> List[ListNode]:
        return self.list_of_depth_with_head_and_tail(tree)[0]

    def list_of_depth_with_head_and_tail(self, tree: TreeNode):
        if tree is None:
            return [], []
        left_head_list, left_tail_list = self.list_of_depth_with_head_and_tail(tree.left)
        right_head_list, right_tail_list = self.list_of_depth_with_head_and_tail(tree.right)
        head_list = [ListNode(tree.val)]
        tail_list = [head_list[0]]
        for depth in range(max(len(left_head_list), len(right_head_list))):
            if depth >= len(left_head_list):
                head_list.append(right_head_list[depth])
                tail_list.append(right_tail_list[depth])
            elif depth >= len(right_head_list):
                head_list.append(left_head_list[depth])
                tail_list.append(left_tail_list[depth])
            else:
                head_list.append(left_head_list[depth])
                tail_list.append(right_tail_list[depth])
                left_tail_list[depth].next = right_head_list[depth]
        return head_list, tail_list


if __name__ == '__main__':
    assert [list_node.get_chain()
            for list_node in Solution().listOfDepth(TreeNode.build_tree([1, 2, 3, 4, 5, None, 7, 8]))] \
           == [[1], [2, 3], [4, 5, 7], [8]]
