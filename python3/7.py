# 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。

class Solution:
    def reverse(self, x: int) -> int:
        reversed = (1 if x >= 0 else -1) * int(str(abs(x))[::-1])
        return reversed if - 1 << 31 <= reversed < 1 << 31 else 0


if __name__ == '__main__':
    assert Solution().reverse(123) == 321
    assert Solution().reverse(-123) == -321
    assert Solution().reverse(120) == 21
