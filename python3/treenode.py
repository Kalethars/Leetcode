from typing import List, Union


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    @classmethod
    def build_tree(cls, nodes: List, index=0) -> Union[None, 'TreeNode']:
        if len(nodes) == 0 or index >= len(nodes) or nodes[index] is None:
            return None
        tree = cls(nodes[index])
        tree.left = cls.build_tree(nodes, index * 2 + 1)
        tree.right = cls.build_tree(nodes, index * 2 + 2)
        return tree

    def vlr(self):  # 前序遍历 pre-order traversal
        return [self.val] + (self.left.vlr() if self.left else []) + (self.right.vlr() if self.right else [])

    def ldr(self):  # 中序遍历 in-order traversal
        return (self.left.ldr() if self.left else []) + [self.val] + (self.right.ldr() if self.right else [])

    def lrd(self):  # 后续变绿 post-order traversal
        return (self.left.lrd() if self.left else []) + (self.right.lrd() if self.right else []) + [self.val]
