# 将一个给定字符串根据给定的行数，以从上往下、从左到右进行 Z 字形排列。
# 比如输入字符串为 "LEETCODEISHIRING" 行数为 3 时，排列如下：
# L   C   I   R
# E T O E S I I G
# E   D   H   N
# 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："LCIRETOESIIGEDHN"。

class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s
        base = numRows * 2 - 2
        result = ''
        for i in range(numRows):
            if i == 0:
                result += s[::base]
            elif i == numRows - 1:
                result += s[numRows - 1::base]
            else:
                s1, s2 = s[i::base], s[base - i::base]
                for j in range(len(s1)):
                    result += s1[j] if j == len(s2) else s1[j] + s2[j]
        return result


if __name__ == '__main__':
    assert Solution().convert('LEETCODEISHIRING', 3) == 'LCIRETOESIIGEDHN'
    assert Solution().convert('LEETCODEISHIRING', 4) == 'LDREOEIIECIHNTSG'
