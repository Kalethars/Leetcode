# 给定一个单词数组和一个长度 maxWidth，重新排版单词，使其成为每行恰好有 maxWidth 个字符，且左右两端对齐的文本。
# 你应该使用“贪心算法”来放置给定的单词；也就是说，尽可能多地往每行中放置单词。必要时可用空格 ' ' 填充，使得每行恰好有 maxWidth 个字符。
# 要求尽可能均匀分配单词间的空格数量。如果某一行单词间的空格不能均匀分配，则左侧放置的空格数要多于右侧的空格数。
# 文本的最后一行应为左对齐，且单词之间不插入额外的空格。
# 说明:
# 单词是指由非空格字符组成的字符序列。
# 每个单词的长度大于 0，小于等于 maxWidth。
# 输入单词数组 words 至少包含一个单词。

from typing import List


class Solution:
    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        text = []
        length, start_index = 0, 0
        for index, word in enumerate(words):
            length += len(word)
            if length + index - start_index > maxWidth:
                spaces, total_words = maxWidth - length + len(word), index - start_index
                if total_words == 1:
                    text.append(words[index - 1] + ' ' * spaces)
                else:
                    length_space, long_spaces = spaces // (total_words - 1), spaces % (total_words - 1)
                    if long_spaces == 0:
                        text.append((' ' * length_space).join(words[start_index: index]))
                    else:
                        text.append(
                            (' ' * (length_space + 1)).join(words[start_index:start_index + 1 + long_spaces]) +
                            (' ' * length_space).join([''] + words[start_index + 1 + long_spaces:index])
                        )
                length, start_index = len(word), index
        text.append(' '.join(words[start_index:]) + ' ' * (maxWidth - length - len(words) + start_index + 1))
        return text


if __name__ == '__main__':
    assert Solution().fullJustify(['This', 'is', 'an', 'example', 'of', 'text', 'justification.'], 16) == \
           ['This    is    an', 'example  of text', 'justification.  ']
    assert Solution().fullJustify(['What', 'must', 'be', 'acknowledgment', 'shall', 'be'], 16) == \
           ['What   must   be', 'acknowledgment  ', 'shall be        ']
    assert Solution().fullJustify(['Science', 'is', 'what', 'we', 'understand', 'well', 'enough', 'to', 'explain',
                                   'to', 'a', 'computer.', 'Art', 'is', 'everything', 'else', 'we', 'do'], 20) == \
           ['Science  is  what we', 'understand      well', 'enough to explain to', 'a  computer.  Art is',
            'everything  else  we', 'do                  ']
